DMUX is an awesome project and it would not of been possible without the following team members, contributors, and softwares:

Contributors
============
| Name               | Role                                     |
|--------------------|------------------------------------------|
| Brigham Keys       | Project leader/Software Developer        |
| Nicolás Ortega     | Software Developer/Translator (Spanish)  |
| CDmir Dammer       | 3D Artist                                |
| Raphael Massabot   | Tester (Arch Linux) / Automated builds   |

SOUND FX
========
| Artist                     | Work                         | License                                                   |
|----------------------------|------------------------------|-----------------------------------------------------------|
| Iwan 'qubodup' Gavovitch   | 3 Explosion Bangs            | Creative Commons Attribution-ShareAlike 4.0 International |
| Qubodup                    | Metal Interactions           | Creative Commons Public Domain                            |

MUSIC
=====
| Artist                  | Work                         | License                                                   |
|-------------------------|------------------------------|-----------------------------------------------------------|
| Human Antagonist        | Floating Paper Bags          | Creative Commons Attribution-ShareAlike 4.0 International |
| Human Antagonist        | Anxious                      | Creative Commons Attribution-ShareAlike 4.0 International |
| Cade Collins            | Goulash                      | Creative Commons Attribution-ShareAlike 4.0 International |
| Wontolla                | Like Cats & Dogs             | Creative Commons Attribution 4.0 International            |
| Gundatsch               | The Destoroya                | Creative Commons Attribution 4.0 International            |
| Gundatsch               | Dungeon of Agony             | Creative Commons Attribution 4.0 International            |
| PlayOnLoop.com          | POL-battle-march-short       | Creative Commons Attribution 4.0 International            |
| Alexandr Zhelanov       | Intro                        | Creative Commons Attribution 4.0 International            |
| Alexandr Zhelanov       | Rock menu track              | Creative Commons Attribution 4.0 International            |
| Alexandr Zhelanov       | 2                            | Creative Commons Attribution 4.0 International            |
| MDuke                   | Trap Music                   | Creative Commons Unported 3.0                             |
| Zander Noriega          | Abelian                      | Creative Commons Unported 3.0                             |
| Matt Thomas             | Boom_boom_boom               | GNU General Public License v3                             |
| The Real Monoton Artist | Metal Song - Energetic       | Creative Commons Public Domain                            |
| Wikimedia               | The Internationale           | Public Domain                                             |


LIBRARIES & TOOLS
=================
| Tool                   | Description                                                                      | License      |
|------------------------|----------------------------------------------------------------------------------|--------------|
| pluto                  | Small utility library for UUIDs, SHA1, and Tiny Encryption                       | GNU GPL v3   |
| SuperTuxKart Exporters | XML exporters used in Blender for Cars, and Scenes                               | GNU GPL v3   |
| VLC                    | used to export .xspf playlist files used internally in the sound server          | GNU GPL v2   |
| Emacs                  | The one true editor                                                              | GNU GPL v3   |
| Vim                    | Used for quick and efficient development                                         | Vim License  |
| Irrlicht               | open source high performance realtime 3D engine                                  | zlib         |
| cAudio                 | 3D Audio Engine Based on Openal                                                  | zlib         |
| Bullet3                | Real-Time Physics Simulation                                                     | zlib         |
| IrrIMGUI               | Irrlicht Binding for IMGUI                                                       | MIT          |
| IMGUI                  | Bloat-free Immediate Mode Graphical User interface                               | MIT          |
| ENet                   | Reliable UDP networking library                                                  | MIT          |
| CppLocate              | provides tools for applications to locate data assets in a cross platform way    | MIT          |
| CMake                  | Cross Platform build system                                                      | BSD 3 Clause |

ARTWORK
=======
| Artist              | Work                           | License                                                   |
|---------------------|--------------------------------|-----------------------------------------------------------|
| CDmir               | El Camino Chassis              | Creative Commons Attribution-ShareAlike 4.0 International |
| CDmir               | El Camino Monstertruck Chassis | Creative Commons Attribution-ShareAlike 4.0 International |
| CDmir               | Tire1-final                    | Creative Commons Attribution-ShareAlike 4.0 International |
| Wikimedia           | Fascist logo                   | Creative Commons Attribution-ShareAlike 4.0 International |
| LWP23D              | Factory_BSW                    | Creative Commons Public Domain                            |
| SuperTuxKart Team   | Olivers Math Class             | Creative Commons Attribution-ShareAlike 4.0 International |
| 0tt0                | Continent Wheel                | Creative Commons Public Domain                            |
| RedHat              | Liberation Fonts               | SIL Open Font License                                     |
| Wikimedia           | Feminism logo                  | Public Domain                                             |
| Wikimedia           | Warsaw Pact logo               | Public Domain                                             |

SoundCloud Links
================
| Artist                              | SoundCloud link                                                                  |
|-------------------------------------|----------------------------------------------------------------------------------|
| Jonathan Holston (Human Antagonist) | https://www.soundcloud.com/humanantagonist                                       |
| Zander Noriega                      | https://www.soundcloud.com/zander-noriega                                        |
| Wontolla                            | https://www.soundcloud.com/iamwontolla                                           |
| Gundatsch                           | https://soundcloud.com/gundatsch/                                                |

SPECIAL THANKS
==============
| Person         | Reasoning                                                                                                   |
|----------------|-------------------------------------------------------------------------------------------------------------|
| GrafZhal       | Made IrrIMGUI and we used his IrrlichtCMake to redo our build system, and we use it for our windows version |
| irrBullet      | The project helped us get started with physics even though we did not end up using it                       |
| StephenLynx    | For being autistic when we needed someone to be                                                             |
| Irrlicht       | For being such an amazing community and giving us such an easy to use 3D engine                             |

LICENSE TEXT
============
| License                                                   | Link to text                                                         |
|-----------------------------------------------------------|----------------------------------------------------------------------|
| GNU General Public License v3                             | https://www.gnu.org/licenses/gpl-3.0.en.html                         |
| Creative Commons Attribution-ShareAlike 4.0 International | https://creativecommons.org/licenses/by-sa/4.0/                      |
| Creative Commons Attribution 4.0 International            | https://creativecommons.org/licenses/by/4.0/                         |
| Creative Commons Unported 3.0                             | https://creativecommons.org/licenses/by/3.0/us/                      |
| Creative Commons Public Domain                            | https://creativecommons.org/publicdomain/zero/1.0/                   |
| zlib                                                      | http://www.zlib.net/zlib_license.html                                |
| BSD 3 Clause                                              | https://cmake.org/licensing/                                         |
| MIT                                                       | https://opensource.org/licenses/MIT                                  |
| SIL Open Font License                                     | http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web  |

If we mistakingly labelled your work as being under the wrong license it was an honest mistake,
please email one of us and tell us the correct license for your work
