###
# Copyright (C) 2016 Nicolas A. Ortega
# License: GNU GPLv3
###
#!/bin/bash
astyle --style=attach \
    --indent=spaces=4 \
    --indent-switches \
    --indent-labels \
    --indent-preproc-define \
    --indent-col1-comments \
    --pad-oper \
    --unpad-paren \
    --align-pointer=name \
    --add-brackets \
    --break-after-logical \
    --lineend=linux \
    src/*.cpp \
    src/*.hpp \
    src/client/*.cpp \
    src/client/*.hpp \
    src/comp/*.cpp \
    src/comp/*.hpp \
    src/gui/*.cpp \
    src/gui/*.hpp \
    src/server/*.cpp \
    src/server/*.hpp \
    src/sys/*.cpp \
    src/sys/*.hpp

rm -rf src/client/*.orig src/server/*.orig src/*.orig src/gui/*.orig src/comp/*.orig src/sys/*.orig

echo "I CAN'T READ THAT CODE!!!"
