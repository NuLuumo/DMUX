set(PLUTO_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib/pluto/include/")

include_directories(
  SYSTEM ${PLUTO_INCLUDE_DIR}
  )

set(PLUTO_LIBRARY "${CMAKE_SOURCE_DIR}/lib/pluto/lib/libplutostatic.a")

set(DMUX_DEPENDENCY_LIBRARIES
  ${DMUX_DEPENDENCY_LIBRARIES}
  ${PLUTO_LIBRARY}
  )
