find_package(OpenGL)
find_package(Irrlicht)

if(WIN32)
  set(IRRLICHT_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib/IrrlichtCMake/include")
endif()

include_directories(
  SYSTEM ${IRRLICHT_INCLUDE_DIR}
  )

set(DMUX_DEPENDENCY_LIBRARIES
  ${DMUX_DEPENDENCY_LIBRARIES}
  ${IRRLICHT_LIBRARY}
  )
