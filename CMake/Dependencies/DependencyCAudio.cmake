find_package(OpenAL)

set(CAUDIO_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib/cAudio/build/include/")
set(CAUDIO_CONFIG_DIR "${CMAKE_SOURCE_DIR}/lib/cAudio/cAudio/include/")

if(WIN32)
  set(OPENAL_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib/openal-soft-1.17.2-bin/include")
  set(OPENAL_LIBRARY "${CMAKE_SOURCE_DIR}/lib/openal-soft-1.17.2-bin/bin/Win32/soft_oal.dll")
endif()

include_directories(
  SYSTEM ${CAUDIO_INCLUDE_DIR}
  SYSTEM ${CAUDIO_CONFIG_DIR}
  SYSTEM ${OPENAL_INCLUDE_DIR}
  )

set(CAUDIO_LIBRARY
  ${CAUDIO_LIBRARY}
  "${CMAKE_SOURCE_DIR}/lib/cAudio/build/cAudio/libcAudio.a"
  )

set(CAUDIO_LIBRARY
  ${CAUDIO_LIBRARY}
  "${CMAKE_SOURCE_DIR}/lib/cAudio/build/DependenciesSource/libvorbis-1.3.2/libVorbis.a"
  )
set(CAUDIO_LIBRARY
  ${CAUDIO_LIBRARY}
  "${CMAKE_SOURCE_DIR}/lib/cAudio/build/DependenciesSource/libogg-1.2.2/libOgg.a"
  )

set(CAUDIO_LIBRARY
  ${CAUDIO_LIBRARY}
  ${OPENAL_LIBRARY}
  )

set(DMUX_DEPENDENCY_LIBRARIES
  ${DMUX_DEPENDENCY_LIBRARIES}
  ${CAUDIO_LIBRARY}
  )
