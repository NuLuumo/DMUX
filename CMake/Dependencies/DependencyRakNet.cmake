set(RAKNET_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib/RakNet/Source")

include_directories(
    SYSTEM ${RAKNET_INCLUDE_DIR})

set(RAKNET_LIBRARY "${CMAKE_SOURCE_DIR}/lib/RakNet/build/Lib/LibStatic/libRakNetLibStatic.a")

set(DMUX_DEPENDENCY_LIBRARIES
    ${DMUX_DEPENDENCY_LIBRARIES}
    ${RAKNET_LIBRARY})
