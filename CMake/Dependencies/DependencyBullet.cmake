find_package(Bullet)

if(WIN32)
  set(BULLET_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib/bullet3/src")
endif()

include_directories(
  SYSTEM ${BULLET_INCLUDE_DIR}
  )

set(DMUX_DEPENDENCY_LIBRARIES
  ${DMUX_DEPENDENCY_LIBRARIES}
  ${BULLET_LIBRARIES}
  )
