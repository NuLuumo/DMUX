set(CMAKE_PREFIX_PATH "${CMAKE_SOURCE_DIR}/lib/cpplocate")

find_package(cpplocate REQUIRED)

set(CPPLOCATE_INCLUDE_DIR
  "${CMAKE_SOURCE_DIR}/lib/cpplocate/source/cpplocate/include/"
  "${CMAKE_SOURCE_DIR}/lib/cpplocate/build/source/cpplocate/include/"
  )

include_directories(SYSTEM ${CPPLOCATE_INCLUDE_DIR})

set(CPPLOCATE_LIBRARY "${CMAKE_SOURCE_DIR}/lib/cpplocate/build/libcpplocate.a")

if(UNIX)
generate_module_info(dmux
    VALUES
    name        "Assets folder"
    version     "0.1"
    description "Directory containing 3D models, textures, music, sound fx, etc."
    author      "CONTRIBUTORS.md"

    BUILD_VALUES
    projectDir   "${PROJECT_SOURCE_DIR}/"
    assetsDir     "${PROJECT_SOURCE_DIR}/assets/"
    musicDir      "${PROJECT_SOURCE_DIR}/assets/sound/music/"
    playlistDir   "${PROJECT_SOURCE_DIR}/assets/sound/playlist/"
    sfxDir        "${PROJECT_SOURCE_DIR}/assets/sound/sfx/"
    chassisDir    "${PROJECT_SOURCE_DIR}/assets/chassis/"
    tireDir       "${PROJECT_SOURCE_DIR}/assets/tire/"
    fontDir       "${PROJECT_SOURCE_DIR}/assets/font/"
    logoDir       "${PROJECT_SOURCE_DIR}/assets/logo/"
    logDir       "/tmp/"
)
elseif(WIN32)
generate_module_info(dmux
    VALUES
    name        "Assets folder"
    version     "0.1"
    description "Directory containing 3D models, textures, music, sound fx, etc."
    author      "CONTRIBUTORS.md"

    BUILD_VALUES
    assetsDir     "${PROJECT_SOURCE_DIR}/assets/"
    musicDir      "${PROJECT_SOURCE_DIR}/assets/music/"
    chassisDir    "${PROJECT_SOURCE_DIR}/assets/chassis/"
    tireDir       "${PROJECT_SOURCE_DIR}/assets/tire/"
    logDir        "${PROJECT_SOURCE_DIR}/log/"
)
file(MAKE_DIRECTORY "${CMAKE_SOURCE_DIR}/log/")
endif()

export_module_info(dmux TARGET dmux FOLDER "build")

set(DMUX_DEPENDENCY_LIBRARIES
    ${DMUX_DEPENDENCY_LIBRARIES}
    ${CPPLOCATE_LIBRARY}
    )
