# The ZLIB license
#
# Copyright (c) 2015 Andr� Netzeband
# Copyright (c) 2016 Brigham Keys, Esq.
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#

set(GCC_LIKE_COMPILER OFF)

if("x${CMAKE_CXX_COMPILER_ID}" STREQUAL "xClang")
	message(STATUS "Detect CLANG compiler and handle it like GCC...")
	set(GCC_LIKE_COMPILER ON)
elseif("x${CMAKE_CXX_COMPILER_ID}" STREQUAL "xGNU")
	message(STATUS "Detect GCC compiler...")
	set(GCC_LIKE_COMPILER ON)
elseif("x${CMAKE_CXX_COMPILER_ID}" STREQUAL "xIntel")
	if(UNIX)
		message(STATUS "Detect Intel compiler and handle it like GCC...")
		set(GCC_LIKE_COMPILER ON)
	endif()
endif()

if(CMAKE_BUILD_TYPE STREQUAL "")
    set(CMAKE_BUILD_TYPE Debug) # TODO: Change this after we have releases going.
endif()

if(GCC_LIKE_COMPILER)
    set(CMAKE_CXX_FLAGS "-pthread -std=c++14 -fno-elide-constructors -pedantic-errors -Werror -Wextra -Wall -Wpedantic -Winit-self -Wmissing-declarations -Wuninitialized -Wfatal-errors")
    if(APPLE)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -arch x86_64")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -arch x86_64 -F/Library/Frameworks -framework Cocoa -framework IOKit")
    endif()
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0")
    set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} -g -O3")
    set(CMAKE_CXX_FLAGS_MINSIZEREL "${CMAKE_CXX_FLAGS_MINSIZEREL} -Os")

	INCLUDE(CheckCSourceCompiles)
	CHECK_C_SOURCE_COMPILES("
		#ifndef __x86_64__
		#error
		#endif

		int main(void) { return 0; }
		"
		WITH_64BIT
		)

	if(WITH_64BIT)
		message(STATUS "Detect 64bit GCC like compiler...")
		set(CMAKE_CL_64 TRUE CACHE BOOL "" FORCE)
		mark_as_advanced(CMAKE_CL_64)
	else()
		message(STATUS "Detect 32bit GCC like compiler...")
		set(CMAKE_CL_64 FALSE CACHE BOOL "" FORCE)
		mark_as_advanced(CMAKE_CL_64)
	endif()
endif()

