#include "Game.hpp"
#ifndef NDEBUG
#include <cassert>
#endif //NDEBUG
#include <thread>
#include <iostream>

#include "sys/World.hpp"

irr::IrrlichtDevice *Game::device = [] {
    irr::SIrrlichtCreationParameters tempParams;
    tempParams.DriverType       = irr::video::EDT_NULL; // No display
    tempParams.Bits             = 32;
    return createDeviceEx(tempParams);
}();

Simulation Game::sim(device);

Game::Game() {
#ifndef NDEBUG
    assert(device not_eq nullptr);
#endif //NDEBUG
}

void Game::run() const {
    gal::sys::createWorld(sim.mir);
    irr::u32 timeStamp = device->getTimer()->getTime();
    irr::u32 deltaTime = 0;

    std::thread input(userInput);

    while(device->run()) {
        sim.step(timeStamp, deltaTime);
    }

    input.join();
}

void Game::userInput() {
    bool quit = false;

    do {
        std::string command;
        std::cin >> command;
        if(command == std::string("exit")) {
            quit = true;
            Game::device->closeDevice();
        } else {
            std::cout << "Invalid command.\n";
        }
    } while(not quit);
}

Game::~Game() {
#ifndef NDEBUG
    assert(device not_eq nullptr);
#endif //NDEBUG
    device->drop();
}
