#pragma once

#include <irrlicht.h>
#include "Server.hpp"
#include "Simulation.hpp"

class Game final {
public:
    Game();
    void run() const;
    ~Game();

    static irr::IrrlichtDevice *device;
    static Simulation sim;

private:
    static void userInput();

    void *operator new(size_t); //!< Preventing heap allocation
    void *operator new[](size_t); //!< Preventing heap allocation
    void operator delete(void *) {} //!< Preventing heap allocation
    void operator delete[](void *) {} //!< Preventing heap allocation
};
