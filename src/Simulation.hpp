#pragma once

#include <irrlicht.h>
#include "comp/World.hpp"
#include "comp/Subject.hpp"

class Simulation {
public:
    Simulation(irr::IrrlichtDevice *parentDevice);
    void step(irr::u32 &timeStamp, irr::u32 &deltaTime);
    ~Simulation();

    static irr::IrrlichtDevice *device;
    static gal::comp::World mir;
    static gal::comp::Subject subject;

private:
    irr::video::SMaterial debugMat;
};
