#pragma once

#include <memory>
#include <string>
#include <irrlicht.h>
#include <tinyxml2.h>
#include <BulletDynamics/Vehicle/btRaycastVehicle.h>

#include "comp/Graphics.hpp"
#include "comp/Physics.hpp"
#include "Observer.hpp"
#include "DmuxCommon.hpp"
#include "Tire.hpp"

/**
 * \brief Vehicle in the gameplay, involves a chassis, wheel set and weapon
 * \details Contains the details for vehicle physics, parses the kart.xml
 * to figure out where the wheel set is located on the vehicle and where
 * the weapon is mounted
 */
class Vehicle final : gal::Observer {

public:
    /**
     * \brief Constructor for Vehicle
     * \details Initializes
     * - Places the Vehicle inside of the specified world
     * - Specifies the chassis to be loaded, eventually it will have the whole vehicle layout in an XML file
     * - Position in the world
     * \param wd The world it is in
     * \param nodePath location of the model file for the Chassis, name of folder inside of /chassis directory
     * \param pos Where the vehicle is going to be spawned
     */
    Vehicle(const char *nodePath,
            const irr::core::vector3df &pos = irr::core::vector3df(btScalar(100.0f), btScalar(10.0f), btScalar(10.0f)));

    /**
     * \brief Updates the Vehicle
     * \details Updates:
     * - Updates the rotation of the tires
     * - Executes event handling
     */
    virtual void notify();

    /**
     * \brief Set the engine force of the vehicle, basically acceleration
     * \param f acceleration rate of vehicle THIS IS A TEMPORARY PUBLIC
     *  INTERFACE FOR TESTING AND IS NOT GUARENTEED TO BE HONORED
     */
    void setEngineForce(const btScalar &f);

    /**
     * \brief Really bad name for this function, but updates the steering value
     * \param s Steering value to be added to vehicle THIS IS A TEMPORARY PUBLIC
     *  INTERFACE FOR TESTING AND IS NOT GUARENTEED TO BE HONORED
     */
    void updateTires(const btScalar s);
    gal::comp::Graphics graphics;

private:
    void initXMLDoc(tinyxml2::XMLDocument
                    &doc); //!< Gets an XML doc for the vehicle
    void parseSceneXML(); //!< Not implemented yet
    irr::core::vector3df getXMLWeaponMount(); //!< Pulls the vector where the weapon is mounted onto the vehicle
    std::string getXMLEngineSound(); //!< gets the filename for the sound played by the vehicles engine
    std::string getXMLKartAttribute(const char *attr); //!< Pulls a kart attribute out of the kart tag in kart.xml

    gal::comp::Physics physics;

    btRaycastVehicle *btVehicle; //!< Bullet vehicle
    btScalar engineForce; //!< The rate of acceleration the engine is driving
    irr::core::vector3df wheelDirectionCS0; //!< Which direction the wheel is facing
    irr::core::vector3df wheelAxleCS; //!< The axis which the wheel rotates arround
    btScalar suspensionRestLength; //!< Gotta figure that one out
    btScalar wheelWidth; //!< How wide the wheels are
    btScalar wheelRadius; //!< How large the wheels are
    btScalar connectionHeight; //!< The height where the wheels are connected to the chassis
    btScalar steer; //!< Current direction the vehicle is steering

    std::string name; //!< name of the vehicle (El-camino, moscovite)
    std::string version;//!< Which version of the vehicle it is
    std::string modelFile; //!< File path for 3D model
    std::string iconFile; //!< Icon to be displayed on the HUD
    std::string
    type; //!< What type of Entity it is, also exposes the directory in the assets/ folder it is in
    std::string groups; //!< What kind of vehicle is it (car, truck, tank)
    std::string engineSound; //!< File path for the sound effect for the engine

    /**
     * \brief Connection point for rear right wheel
     * \details Parsed from the respective kart.xml which contains the information
     */
    irr::core::vector3df rearRightWheelConnection;

    /**
     * \brief Connection point for wheel
     * \details Parsed from the respective kart.xml which contains the information
     */
    irr::core::vector3df rearLeftWheelConnection;

    /**
     * \brief Connection point for wheel
     * \details Parsed from the respective kart.xml which contains the information
     */
    irr::core::vector3df frontLeftWheelConnection;

    btRaycastVehicle::btVehicleTuning tuning; //!< vehicle tuning

    /**
     * \brief Connection point for wheel
     * \details Parsed from the respective kart.xml which contains the information
     */
    irr::core::vector3df frontRightWheelConnection;

    Tire t;
    //Could perhaps be put inside of a pair of pairs like the tireset
    irr::scene::IAnimatedMeshSceneNode *rearRightWheel;  //!< Irrlicht node for rear right wheel (Needs to be an entity)
    irr::scene::IAnimatedMeshSceneNode *rearLeftWheel;   //!< Irrlicht node for rear left wheel (Needs to be an entity)
    irr::scene::IAnimatedMeshSceneNode *frontLeftWheel;  //!< Irrlicht node for front left wheel(Needs to be an entity)
    irr::scene::IAnimatedMeshSceneNode *frontRightWheel; //!< Irrlicht node for front right wheel (Needs to be an entity)

    irr::core::vector3df getXMLWheelPos(const char *a);  //!< Gets a wheel position from kart.xml, used in the initializer list


    irr::core::vector3df weaponMountPoint;  //!< Point on vehicle where the weapon is mounted

    void operator delete(void *) {} //!< No pointers!

    btWheelInfoConstructionInfo wheel; //!< Information about the vehicles wheels

    void addWheels(btRaycastVehicle *vehicle,
                   btRaycastVehicle::btVehicleTuning tuning); //!< adds the wheels to the btVehicle
    btRaycastVehicle::btVehicleTuning m_tuning; //!< Vehicle tuning

    btVehicleRaycaster *vehicleRayCaster; //!< bullet data type for handling vehicle physics
    static std::string chassisDir; //!< asset path for the chassis
    static std::string tireDir; //!< asset path for the tires
};
