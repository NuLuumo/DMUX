#pragma once
#include <irrTypes.h>
/**
 * \brief Used in identifying packets sent & received.
 * \details Each of these enumerations represent a different packet type
 * which is used by the network handlers (on both the client and the server)
 * to distinguish packet types from one another.
 */
enum {
    ACTIONS_KEYS_PRESSED    = 0x00, //!< Packet will contain an array stating if action keys are pressed or not
    GUN_ROTATION            = 0x01, //!< Packet will contain the rotation of the mounted gun
    ENTITY_LIST             = 0x02, //!< Packet will contain a list of entities in game
    SERV_LIST_REQUEST       = 0x03, //!< Packet for requesting the server list from the master server
    SERV_LIST               = 0x04 //!< Packet contains list of servers
};

/**
 * \brief Used to attach user specified key press to an action.
 * \details In order to send key presses to the server in such a way that everyone
 * can have their own custom key presses these action keys are sent in its stead.
 */
enum {
    ACCEL_FORWARD           = 0x00, //!< Forward key is pressed
    ACCEL_REVERSE           = 0x01, //!< Backward key is pressed
    STEER_LEFT              = 0x02, //!< Left key is pressed
    STEER_RIGHT             = 0x03, //!< Right key is pressed
    HEADLIGHT_TOGGLE        = 0x04, //!< Light key is pressed
    ENABLE_CHAT             = 0x05, //!< A client has enabled in game chat
    SEND_CHAT_MESSAGE       = 0x06, //!< A client has sent a chat message to the server
    NUM_ACTION_KEYS         = 0x07 //!< The total number of action keys (used for array sizes and such)
};
/**
 * \brief Channel identifiers.
 * \details These enumerations are used to avoid having to refer to channels directly.
 */
enum {
    CHANNEL_CHAT            = 0x00, //!< Chat messages will be passed on this channel
    CHANNEL_GAMEPLAY        = 0x01, //!< action keys and the massive server packet will be passed on this channel
    CHANNEL_ROTATION        = 0x02, //!< The camera rotation of the client will be passed on this channel
    NUM_CHANNELS            = 0x03 //!< The total number of channels
};

#pragma pack(push, 1)
/**
 * \brief Packet for action keys.
 * \details This packet contains the action keys that are and are not pressed
 * in order to send such information to the server.
 */
struct ActionKeysPacket {
    //unsigned char useTimeStamp;
    //RakNet::Time timeStamp;
    unsigned char typeId;
    // DATA:
    bool actionKeys[NUM_ACTION_KEYS];
};
#pragma pack(pop)

/**
 * \brief Used to create a packet for action keys.
 * \details Function used to create a char pointer packet that can be sent to
 * the server without any further trouble.
 * @param[in] actionKeysDown The action keys that will be put in the packet
 * \return A char pointer packet that can be sent to the server via RakNet.
 */
char *createActionKeysPacket(bool actionKeysDown[NUM_ACTION_KEYS]);

#pragma pack(push, 1)
/**
 * \brief Packet for gun rotation.
 * \details This packet contains the yaw, pitch, and rotation of the gun in
 * order to send such information to the server.
 */
struct GunRotationPacket {
    //unsigned char useTimeStamp;
    //RakNet::Time timeStamp;
    unsigned char typeId;
    // DATA:
    irr::f32 x, y, z;
};
#pragma pack(pop)

/**
 * \brief Used to create a packet for the gun's rotation.
 * \details Function used to create a char pointer packet that can be sent to
 * the server without any further trouble.
 * @param[in] x The yaw of the rotation.
 * @param[in] y The pitch of the rotation.
 * @param[in] z The roll of the rotation.
 * \return A char pointer that can be sent to the server via RakNet.
 */
char *createGunRotationPacket(irr::f32 x, irr::f32 y, irr::f32 z);
