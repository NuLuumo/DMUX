#pragma once
#include <irrlicht.h>
#include <LinearMath/btVector3.h>
#include <string>

irr::core::vector3df stringToIrrVector(const std::string &v); //!< converts 3 floating values in a string seperated by white space into an Irrlicht vector
void getMeshSize(const irr::scene::IAnimatedMeshSceneNode *mesh); //!< get the 3D dimensions of a mesh
irr::core::vector3df btVectorToIrr(const btVector3 &vec);
bool fileExists(const char *name); //!< quick function to see if file exists
btVector3 irrToBtVector(const irr::core::vector3df &vec); //!< conversion function
