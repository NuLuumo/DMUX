#pragma once


/**
 * @file   World.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  System for bullet world
 *
 * Copyright (c) 2016 Collective Tyranny
 */

namespace gal {
namespace comp {
class World;
} // comp
} // gal

namespace gal {
namespace sys {
void createWorld(gal::comp::World &world);
void updateScreen(gal::comp::World &world);
} // sys
} // gal
