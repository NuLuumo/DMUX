#include "Graphics.hpp"

#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>
#include "Simulation.hpp"
#include "DmuxCommon.hpp"

namespace gal {
namespace sys {
void addNode(comp::Graphics &graphics) {
    graphics.modelPath = graphics.assetsDir + graphics.objType + std::string("/") + graphics.objStr + std::string("/") + graphics.objStr + std::string(".obj");

    if(not fileExists(graphics.modelPath.c_str())) {
        //try for b3d
        graphics.modelPath = graphics.assetsDir + graphics.objType + std::string("/") +
                             graphics.objStr + std::string("/") + graphics.objStr + std::string(".b3d");
    }

#ifndef NDEBUG
    //assert the contents of Mir, not Mir itself
#endif

    if(not graphics.objStr.empty()) {
        graphics.node = Simulation::device->getSceneManager()->addMeshSceneNode(
                            Simulation::device->getSceneManager()->getMesh(graphics.modelPath.c_str()));

#ifndef NDEBUG
        assert(Simulation::device not_eq nullptr);
        assert(graphics.node not_eq nullptr);
#endif
        if(graphics.objType == std::string("tracks")) {
            graphics.node->setScale(irr::core::vector3df(4.0f, 4.0f, 4.0f));
        }

        graphics.node->setMaterialFlag(irr::video::EMF_LIGHTING, true);
        graphics.node->setAutomaticCulling(true);
        graphics.node->setMaterialFlag(irr::video::EMF_BACK_FACE_CULLING, false);
    }


}
} // sys
} // gal
