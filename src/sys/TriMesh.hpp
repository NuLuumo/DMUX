#pragma once

/**
 * @file   TriMesh.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Component for building trimeshes with bullet physics
 *
 * Copyright (c) 2016 Collective Tyranny
 */

class btTriangleMesh;
class btCollisionShape;
class btBvhTriangleMeshShape;
class btConvexTriangleMeshShape;
class btConvexHullShape;

namespace irr {
namespace scene {
class IMesh;
} // scene
} // irr

namespace gal {
namespace sys {

btConvexHullShape *buildConvexHullShape(irr::scene::IMesh *mesh, bool simplify = true);
btTriangleMesh *buildTriMesh(irr::scene::IMesh *mesh);

template <class MeshShape> btCollisionShape *buildCollisionShape(irr::scene::IMesh *mesh, MeshShape *meshShape) {
    meshShape = new MeshShape(buildTriMesh(mesh), true);
    return meshShape;
}
} // sys
} // gal
