#pragma once

/**
 * @file   Graphics.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Graphics system for Irrlicht
 *
 * Copyright (c) 2016 Collective Tyranny
 */
#include "comp/Graphics.hpp"

namespace gal {
namespace sys {
void addNode(comp::Graphics &graphics);
} // sys
} // gal
