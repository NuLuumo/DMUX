#pragma once

/**
 * @file   Physics.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @author Nicolas Ortega (nicolas.ortega.froysa@gmail.com)
 * @brief  system for handling bullet physics
 *
 * Copyright (c) 2016 Collective Tyranny
 */

namespace gal {
namespace comp {
class Physics;
} // comp
} // gal
namespace gal {
namespace sys {
void addToWorld(comp::Physics &physics);
} // sys
} // gal
