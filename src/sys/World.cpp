#include "World.hpp"
#include "comp/World.hpp"

#include <btBulletDynamicsCommon.h>
#include <ISceneNode.h>

namespace gal {
namespace sys {
void updateRender(const btRigidBody *object);
void createWorld(gal::comp::World &world) {

    world.collisionConfiguration = new btDefaultCollisionConfiguration();
    world.dispatcher = new btCollisionDispatcher(world.collisionConfiguration);
    world.pairCache  = new btHashedOverlappingPairCache();
    world.broadphase = new btAxisSweep3(btVector3(-1000, -1000, -1000), btVector3(1000, 1000, 1000), 3500, world.pairCache);
    world.solver     = new btSequentialImpulseConstraintSolver();
    world.world      = new btDiscreteDynamicsWorld(world.dispatcher, world.broadphase, world.solver, world.collisionConfiguration);
    world.world->setGravity(btVector3(0, -10, 0));

#ifndef NDEBUG
    assert(world.collisionConfiguration not_eq nullptr);
    assert(world.dispatcher not_eq nullptr);
    assert(world.broadphase not_eq nullptr);
    assert(world.solver not_eq nullptr);
    assert(world.world not_eq nullptr);
#endif
}

void updateScreen(gal::comp::World &world) {

    for(std::vector<btRigidBody *>::iterator iter = world.worldObjs.begin(); iter not_eq world.worldObjs.end(); ++iter) {
        updateRender(*iter);
    }
}

void updateRender(const btRigidBody *object) {
    irr::scene::ISceneNode *node = static_cast<irr::scene::ISceneNode *>(object->getUserPointer());

    // Set position
    btVector3 point = object->getCenterOfMassPosition();

    node->setPosition(irr::core::vector3df(point[0], point[1], point[2]));

    // Set rotation
    irr::core::vector3df Euler;
    const btQuaternion &tQuat = object->getOrientation();
    irr::core::quaternion q(tQuat.getX(), tQuat.getY(), tQuat.getZ(), tQuat.getW());
    q.toEuler(Euler);
    Euler *= irr::core::RADTODEG;
    node->setRotation(Euler);
}
} // sys
} // gal
