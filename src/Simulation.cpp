#include "Simulation.hpp"
#include "sys/World.hpp"
#ifndef NDEBUG
#include <cassert>
#endif //NDEBUG
#include "DebugDraw.hpp"

irr::IrrlichtDevice *Simulation::device;
gal::comp::World Simulation::mir;
gal::comp::Subject Simulation::subject;

Simulation::Simulation(irr::IrrlichtDevice *parentDevice) {
    device = parentDevice;
#ifndef NDEBUG
    assert(device not_eq nullptr);
    //assert the contents of Mir, not Mir itself
#endif //NDEBUG
    mir.isDebugDraw = false;
    debugMat.Lighting = false;
}

void Simulation::step(irr::u32 &timeStamp, irr::u32 &deltaTime) {
    mir.update();
    gal::sys::updateScreen(mir);
    deltaTime = (device->getTimer()->getTime() - timeStamp);
    timeStamp = device->getTimer()->getTime();

#ifndef NDEBUG
    assert(mir.world not_eq nullptr);
#endif //NDEBUG

    mir.world->stepSimulation(deltaTime * 0.001f, 60);

    if(mir.isDebugDraw) {
        device->getVideoDriver()->setMaterial(debugMat);
        device->getVideoDriver()->setTransform(irr::video::ETS_WORLD,
                                               irr::core::IdentityMatrix);
        mir.world->debugDrawWorld();
    }
}

Simulation::~Simulation() {
}
