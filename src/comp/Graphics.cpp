#include "Graphics.hpp"
#include <ISceneNode.h>
#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>

namespace gal {
namespace comp {
std::string Graphics::assetsDir = cpplocate::findModule("dmux").value("assetsDir");
} // comp
} // gal
