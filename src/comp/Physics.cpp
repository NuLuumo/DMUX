#include "Physics.hpp"
#include "Simulation.hpp"

namespace gal {
namespace comp {
Physics::~Physics() {
    Simulation::mir.collisionShapes.remove(shape);
    delete motionState;
    delete shape;
    delete rigidBody;
}
} // comp
} // gal
