#pragma once

/**
 * @file   World.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Container class for WorldData
 *
 * Copyright (c) 2016 Collective Tyranny
 */

#include <map>
#include <string>
#include <vector>
#include <LinearMath/btAlignedObjectArray.h>
#include "comp/Subject.hpp"

class btDefaultCollisionConfiguration;
class btCollisionDispatcher;
class btHashedOverlappingPairCache;
class btBroadphaseInterface;
class btSequentialImpulseConstraintSolver;
class btDiscreteDynamicsWorld;
class btRigidBody;
class btCollisionShape;

namespace gal {
class Observer;
}

namespace gal {
namespace comp {

/**
 * \brief Container for all the Entities in the gameplay
 */
class World final {

public:
    ~World();
    void update(); //!< updates all of the entities inside of the world data

    Subject subject;
    bool isDebugDraw;
    //Bullet World
    btDefaultCollisionConfiguration *collisionConfiguration;
    btCollisionDispatcher *dispatcher;
    btHashedOverlappingPairCache *pairCache;
    btBroadphaseInterface *broadphase;
    btSequentialImpulseConstraintSolver *solver;
    btDiscreteDynamicsWorld *world;
    std::vector<btRigidBody *> worldObjs;
    btAlignedObjectArray<btCollisionShape *> collisionShapes;
private:
    void *operator new(size_t);     //!< Preventing heap allocation
    void *operator new[](size_t);   //!< Preventing heap allocation
    void   operator delete(void *) {}    //!< Preventing heap allocation
    void   operator delete[](void *) {} //!< Preventing heap allocation
    void updateRender(const btRigidBody *object);
};
} // comp
} // gal
