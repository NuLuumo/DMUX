#include "World.hpp"
#include "Simulation.hpp"
#include "sys/World.hpp"
#include "Observer.hpp"

#include <btBulletDynamicsCommon.h>

#include <cstddef>
#include <string>
#include "Logger.hpp"

namespace gal {
namespace comp {

void World::update() {

    for(std::vector<std::string>::const_iterator it = subject.kr.begin(),
            end = subject.kr.end(); it != end; ++it) {
        subject.census[*it]->notify();
    }

}

World::~World() {
    //delete collision shapes
    for(int j = 0; j < collisionShapes.size(); j++) {
        btCollisionShape *shape = collisionShapes[j];
        if(shape == nullptr) {
            delete shape;
        }
    }
    collisionShapes.clear();
    delete world;
    delete solver;
    delete broadphase;
    delete dispatcher;
    delete collisionConfiguration;
}
} // comp
} // gal
