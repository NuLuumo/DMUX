#pragma once

/**
 * @file   Physics.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Physics component for bullet physics
 *
 * Copyright (c) 2016 Collective Tyranny
 */

#include <btBulletDynamicsCommon.h>
#include <IMesh.h>

namespace gal {
namespace comp {
struct Physics {
    btScalar mass; //!< Floating point representation of the mass
    btTransform transform; //!< Set the initial position of the object
    btDefaultMotionState *motionState; //!< motionstate used in the creation of the entity
    btCollisionShape *shape; //!< Collision shape for the entity
    btVector3 localInertia; //!< Inertia of the rigid body upon spawning
    /**
     * \brief Bullet Rigid body for physics
     * \details Holds the collision shape and enables collision detection for
     * the Entity
     */
    btRigidBody *rigidBody;
    btVector3 position;
    irr::scene::IMesh *mesh;

    ~Physics();
};
} // comp
} // gal
