#pragma once

/**
 * @file   Subject.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Component for Subject
 *
 * Copyright (c) 2016 Collective Tyranny
 */

#include <map>
#include <vector>
namespace gal {
class Observer;
}

namespace gal {
namespace comp {
struct Subject {
    std::map <std::string, Observer *> census; //!< Holds references to each Entity in the world
    std::vector<std::string> kr; //!< KeyRing, holds the ID for each Citizen in the census
};
} //comp
} //gal
