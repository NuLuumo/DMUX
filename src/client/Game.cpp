#include <thread>
#include <cassert>
#include <string>
#include <fstream>
#include <tinyxml2.h>
#include <IrrIMGUI/SIMGUISettings.h>

#include "sys/World.hpp"
#include "gui/Menu.hpp"
#include "Game.hpp"
#include "Player.hpp"
#include "Vehicle.hpp"
#include "comp/World.hpp"
#include "DebugDraw.hpp"
#include "Simulation.hpp"

Settings Game::playerSettings;
irr::IrrlichtDevice *Game::device = createDeviceEx(playerSettings.getIrrlichtParams());
Simulation Game::sim(device);
DebugDraw *debugDraw; //!< Debug Drawer for world
menu::Menu *Game::menu;

Game::Game() :
    drawWireFrame(false),
    jBox() {
    device->setWindowCaption(L"DMUX");
    device->getSceneManager()->addLightSceneNode(0, irr::core::vector3df(20, 60,
            -50), irr::video::SColorf(1.0f, 1.0f, 1.0f), 2000.0f);
    device->getCursorControl()->setVisible(false);
#ifndef NDEBUG
    assert(device not_eq nullptr);
#endif
}

void Game::run() const {
    gal::sys::createWorld(sim.mir);
    debugDraw = new DebugDraw(Simulation::device);
    debugDraw->setDebugMode(
        btIDebugDraw::DBG_DrawWireframe |
        btIDebugDraw::DBG_DrawAabb |
        btIDebugDraw::DBG_DrawContactPoints |
        //btIDebugDraw::DBG_DrawText |
        //btIDebugDraw::DBG_DrawConstraintLimits |
        btIDebugDraw::DBG_DrawConstraints //|
    );

#ifndef NDEBUG
    assert(debugDraw not_eq nullptr);
#endif
    sim.mir.world->setDebugDrawer(debugDraw);

    device->getVideoDriver()->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, true);

    // Set our delta time and time stamp
    irr::u32 timeStamp = device->getTimer()->getTime();
    irr::u32 deltaTime = 0;
    std::thread jukeBoxThread(JukeBox::jukeBoxLoop);

#ifndef NDEBUG
    //running our unit tests before we go into the game
    assert(sim.mir.collisionConfiguration not_eq nullptr);
    assert(sim.mir.dispatcher not_eq nullptr);
    assert(sim.mir.broadphase not_eq nullptr);
    assert(sim.mir.solver not_eq nullptr);
    assert(sim.mir.world not_eq nullptr);
    assert(device not_eq nullptr);
#endif

    menu = new menu::Menu();

    //Main menu loop
    {
        while(menu->run() and device->run()) {
            device->getVideoDriver()->beginScene(true, true, irr::video::SColor(0, 0, 101, 140));
            menu->showMainMenu();
            device->getVideoDriver()->endScene();
        }
    }

    //    menu.closeMenu();

    //Game loop
    device->getCursorControl()->setVisible(false);

    {
        Player b;
        Arena a;
        device->setEventReceiver(&b);
        //	JukeBox::setPlayList("olivermath"); //needs to be moved into Arena
        while(device->run()) {
            device->getVideoDriver()->beginScene(true, true, irr::video::SColor(0, 0, 101, 240));
            sim.step(timeStamp, deltaTime);

            device->getSceneManager()->drawAll();
            b.show();
            device->getVideoDriver()->endScene();
        }
    }
    JukeBox::closed = true;
    jukeBoxThread.join();
}

Game::~Game() {
#ifndef NDEBUG
    assert(debugDraw not_eq nullptr);
    assert(device not_eq nullptr);
#endif
    delete menu;
    delete debugDraw;
    device->drop();
}
