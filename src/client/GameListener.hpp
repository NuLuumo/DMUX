#pragma once
#include <irrlicht.h>
#include <IrrIMGUI/IncludeIMGUI.h>
#include <IrrIMGUI/IrrIMGUI.h>
#include <fstream>
#include <chrono>
#include <ctime>
#include "Logger.hpp"

/**
 * @file   GameInput.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @author Nicolas Ortega (nicolas.ortega.froysa@gmail.com)
 * @brief  Receives and handles events for GUI and player input
 *
 * Copyright (c) 2016 Collective Tyranny
 */


/**
 * \brief Receives and handles events for GUI and player input
 * \details Inherits from the IrrIMGUI event handles so we can
 * also interface with the GUI engine we are using since it is
 * external from Irrlicht.
 */
class GameInput final : public IrrIMGUI::CIMGUIEventReceiver, public Logger {
public:

    /**
     * \brief Constructor for Event handler
     * \details Initializes all keys as not being pressed
     */
    GameInput();

    /**
     * \brief function called whenever the player has any kind of input
     * \details Handles events involving the GUI and the gameplay, there
     * is a toggle between modes for the GUI and for the gameplay so the
     * handling of events are independant for both
     * \param event The player event detected by Irrlicht
     */
    bool OnEvent(const irr::SEvent &event);

    /**
     * \brief test whether a key is down
     * \param keyCode The keycode being checked to see if it is active
     * \returns whether the key is pressed
     */
    bool isKeyDown(const irr::EKEY_CODE &keyCode) const;

private:
    /**
     * \brief Array holding all the activated keys
     */
    bool keyIsDown[irr::KEY_KEY_CODES_COUNT];
};
