#pragma once

/**
 * @file   Game.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @author Nicolas Ortega (nicolas.ortega.froysa@gmail.com)
 * @brief  Game class that is the parent of all other elements of DMUX
 *
 * Copyright (c) 2016 Collective Tyranny
 */

#include <memory>
#include <map>
#include <irrlicht.h>
#include <IrrIMGUI/IncludeIMGUI.h>
#include <IrrIMGUI/IrrIMGUI.h>
#include <IrrIMGUI/IrrIMGUIDebug.h>

#include "DebugDraw.hpp"
#include "Simulation.hpp"
#include "JukeBox.hpp"
#include "Client.hpp"
#include "Player.hpp"
#include "gui/Settings.hpp"

namespace menu {
class Menu;
}

/**
 * \brief Your typical game class
 * \details Container class for all the objects in DMUX
 */
class Game final {

public:

    /**
     * \brief Default constructor for DMUX
     * \details Initializes:
     * - debug drawing properties
     * - jukebox for sound
     * - networking
     * - loads the user's settings file
     */
    Game();

    /**
     * \brief game loop for DMUX
     * \details Does some initialization of it's own, puts
     * elements into threads, and then runs the game loop
     */
    void run() const;

    ~Game();

    /**
     * \brief Irrlicht device
     * \details Please look this up on the Irrlict docs
     */
    static irr::IrrlichtDevice *device;
    static Simulation sim;

    static Settings playerSettings;

    static menu::Menu *menu;

private:
    static std::string settingsDir;
    const bool drawWireFrame; //!< Whether or not to draw wireframes in debug draw mode
    const JukeBox jBox; //!< Sound Server for the game

    void *operator new(size_t);     //!< Preventing heap allocation
    void *operator new[](size_t);   //!< Preventing heap allocation
    void  operator delete(void *) {}    //!< Preventing heap allocation
    void  operator delete[](void *) {} //!< Preventing heap allocation
};
