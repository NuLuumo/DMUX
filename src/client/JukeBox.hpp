#pragma once

/**
 * @file   JukeBox.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Sound server in DMUX based on the cAudio OpenAL wrapper
 * The track playing occurs on it's own thread
 *
 * Copyright (c) 2016 Collective Tyranny
 */

#include <cAudio.h>
#include <thread>
#include <vector>
#include <algorithm>
#include <string>
#include <irrTypes.h>
#include "Logger.hpp"
/**
 * \brief Information about track
 * \details Holds the ID3 information pulled by the xpsf generator
 */
struct Record final {

    std::string location;    //!< \brief file location
    std::string title;    //!< \brief Name of the song
    std::string creator;    //!< \brief Creator of the record

private:
    void *operator new(size_t);     //!< Preventing heap allocation
    void *operator new[](size_t);   //!< Preventing heap allocation
    void   operator delete(void *) {}    //!< Preventing heap allocation
    void   operator delete[](void *) {} //!< Preventing heap allocation
};

/**
 * \brief Sound Server used in DMUX but could really be used anywhere else
 * \details Parses a XSPF playlist that we use VLC to export, this makes
 * coming up with new playlists for DMUX very easy since the songs would
 * already be up in our media player. This also allows us to specify songs on
 * a per scene basis so scenes can share music but have their own playlist
 */
class JukeBox final : public Logger {

public:
    /**
     * \brief Constructor for JukeBox
     * \details Initializes the JukeBox
     * - Opens the JukeBox
     * - Initializes cAudio
     * - Lists available devices, including default device
     * - Reads the playlist file
     */
    JukeBox();
    ~JukeBox();

    static bool closed; //!< \brief If the JukeBox is running

    /**
     * \brief
     * \details initializes a thread for the song so it runs independantly
     * It also grabs a random record from the playlist
     */
    static void jukeBoxLoop();

    /**
     * \brief Plays a file at a given location
     * \param f File containing the souund effect
     */
    static void playFX(const char *f);
    static void setPlayList(const char *pl);

private:
    static void playSong();                 //!< Play next song in playlist
    static cAudio::IAudioManager *audioMgr; //!< cAudio handler
    static std::vector<std::thread> sfx;    //!< Thread the sound is played on should be changed to cAudio thread
    static std::vector<Record> records;     //!< All the available songs in the playlist
    std::thread soundThread; //!< change me to cAudio thread
    void readPlayListXML();     //!< Parses the xspf file and gets the available songs for a track
    void *operator new(size_t);     //!< Preventing heap allocation
    void *operator new[](size_t);   //!< Preventing heap allocation
    void  operator delete(void *) {}    //!< Preventing heap allocation
    void  operator delete[](void *) {} //!< Preventing heap allocation
    static std::string musicDir;
    static std::string playlistDir;
    static std::string sfxDir;
    static std::string loadedPlaylist;
    static bool changePlaylist;
    static irr::f32 musicVolume;
    static irr::f32 masterVolume;
    static irr::f32 soundfxVolume;
};
