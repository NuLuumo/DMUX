#include "Player.hpp"

#include "Game.hpp"
#include "Simulation.hpp"
#include "Observer.hpp"
#include "gui/Menu.hpp"
#include <cassert>
#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>

btScalar yaw;
btScalar pitch;

Player::Player() :
    Gui(),
    Observer(),
    isTeamSelected(false), //used for team selection
    v("el-camino-monstertruck", irr::core::vector3df(0, 20, 0)),
    isInPauseMenu(false),
    isInChat(false) {

    chatText.push_back(std::string("Chat Text Here: "));

    //settings available teams, eventually we will
    //get these in the form of a packet from the server
    availableTeams.push_back("Fascist");
    availableTeams.push_back("Communist");
    availableTeams.push_back("Feminist");

    for(int i = 0; i < irr::KEY_KEY_CODES_COUNT; ++i) {
        keyIsDown[i] = false;
    }

    for(int i = 0; i < NUM_ACTION_KEYS; ++i) {
        actionKeysDown[i] = oldActionKeysDown[i] = false;
    }

#ifndef NDEBUG
    assert(Game::device not_eq nullptr);
#endif //NDEBUG

    camera = Game::device->getSceneManager()->addCameraSceneNode();
    camera->setPosition(irr::core::vector3df(0, 30, 200));
    lastSendTime = std::clock();
    discover(Simulation::mir.subject);
    imguiSettings->mIsGUIMouseCursorEnabled = false;
    init();
}

void Player::show() {

    pGUI->startGUI();

    if(not isTeamSelected) {
        Game::device->getCursorControl()->setVisible(true);
        isTeamSelected = Game::menu->showTeamSelection();
        //At some point we want to remove the system cursor all together
        //and only use a special in game cursor
    }

    if(isInPauseMenu) {
        Game::device->getCursorControl()->setVisible(true);
        isInPauseMenu = Game::menu->showPauseMenu();
    }

    ImGui::SetNextWindowPos(ImVec2(navWindowPos.first, navWindowPos.second));
    ImGui::SetNextWindowSize(ImVec2(navWindowSize.first, navWindowSize.second));

    ImGui::Begin("Chat Box", nullptr, ImGuiWindowFlags_NoTitleBar);
    for(const auto &text : chatText) {
        ImGui::TextWrapped(text.c_str());
    }
    if(isInChat) {
        ImGui::InputText("Chat: ", const_cast<char *>(chatMessage.data()), 64);
    }
    ImGui::End();

    pGUI->drawAll();
}

bool Player::OnEvent(const irr::SEvent &event) {
    GuiHandle(event);

    if(isInChat) {
        char inputBuffer[256] = {};
        wctomb(inputBuffer, event.KeyInput.Char);
        switch(event.EventType) {
            case irr::EET_KEY_INPUT_EVENT:
                if(event.KeyInput.PressedDown) {
                    chatMessage.append(inputBuffer);
                }
                keyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
                return true;

            default:
                break;
        }
    } else {
        switch(event.EventType) {
            case irr::EET_KEY_INPUT_EVENT:
                keyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
                return true;
            case irr::EET_LOG_TEXT_EVENT:
                return true;
            default:
                break;
        }
    }
    return false;
}

bool Player::isKeyDown(const irr::EKEY_CODE &keyCode) const {
    return keyIsDown[keyCode];
}

void Player::notify() {

    if(Game::device->isWindowActive()) {
        if(not isInPauseMenu) {
            if(isTeamSelected) {
                moveCameraControl();
            }
        }
    }

    btScalar interval = (std::clock() - lastSendTime) /
                        static_cast<double>(CLOCKS_PER_SEC);
    if(interval >= 0.03) {
        // TODO: Send the cam rotation
    }

    if(isKeyDown(irr::KEY_ESCAPE)) {
        if(isInPauseMenu) {
            Game::device->getCursorControl()->setVisible(false);
            //        imguiSettings->mIsGUIMouseCursorEnabled = false;
            isInPauseMenu = false;
        } else {
            Game::device->getCursorControl()->setVisible(true);
            //        imguiSettings->mIsGUIMouseCursorEnabled = true;
            isInPauseMenu = true;
        }
    }

    updateActionKeys();

    btScalar accelForce = 0.0f;
    accelForce += actionKeysDown[ACCEL_FORWARD] ? 10000.0f : 0;
    accelForce -= actionKeysDown[ACCEL_REVERSE] ? 10000.0f : 0;
    v.setEngineForce(accelForce);

    btScalar steer = 0.0f;
    steer += actionKeysDown[STEER_RIGHT] ? 0.01f : 0;
    steer -= actionKeysDown[STEER_LEFT] ? 0.01f : 0;
    v.updateTires(steer);

    if(actionKeysDown[ENABLE_CHAT]) {
        isInChat = true;
    }

    if(isKeyDown(irr::KEY_RETURN)) {
        if(isInChat) {
            isInChat = false;
            chatMessage.clear();
        }
        actionKeysDown[SEND_CHAT_MESSAGE] = true;
    } else {
        actionKeysDown[SEND_CHAT_MESSAGE] = false;
    }

    // If the action keys pressed have changed then send a packet
    for(unsigned short i = 0; i < NUM_ACTION_KEYS; ++i) {
        if(oldActionKeysDown[i] not_eq actionKeysDown[i]) {
            for(unsigned short i = 0; i < NUM_ACTION_KEYS; ++i) {
                oldActionKeysDown[i] = actionKeysDown[i];
            }
            break;
        }
    }
}

inline void Player::updateActionKeys() {
    actionKeysDown[ACCEL_FORWARD] = isKeyDown(irr::KEY_KEY_W);
    actionKeysDown[ACCEL_REVERSE] = isKeyDown(irr::KEY_KEY_S);
    actionKeysDown[STEER_LEFT] = isKeyDown(irr::KEY_KEY_A);
    actionKeysDown[STEER_RIGHT] = isKeyDown(irr::KEY_KEY_D);
    actionKeysDown[HEADLIGHT_TOGGLE] = isKeyDown(irr::KEY_KEY_F);
    actionKeysDown[ENABLE_CHAT] = isKeyDown(irr::KEY_KEY_T);
}

Player::~Player() { }

inline void Player::moveCameraControl() const {
    // Get the cursor's current position
    irr::core::position2d<irr::f32> cursorPos = Game::device->getCursorControl()->
            getRelativePosition();

    // Check to see how much its position has changed
    btScalar changeX = (cursorPos.X - 0.5) * 256.0f;
    btScalar changeY = (cursorPos.Y - 0.5) * 256.0f;

    // Change the yaw & pitch based on the change in X & Y of the cursor
    yaw += changeX;
    pitch += changeY;
    // Set limits to pitch
    if(pitch < -45) {
        pitch = -45;
    } else if(pitch > 45) {
        pitch = 45;
    }
    // We don't want any numbers lower than 0 or higher than 360
    if(yaw > 360) {
        yaw -= 360;
    } else if(yaw < 0) {
        yaw += 360;
    }

    // Get the vehicle position
    const irr::core::vector3df playerPos = v.graphics.node->getPosition();

    // Calculate where the camera should be placed (yaw and pitch based)
    btScalar xf = playerPos.X - (sin(yaw * M_PI / 180.0f) *
                                 cos(pitch * M_PI / 180.0f)) * 16.0f;
    btScalar yf = playerPos.Y + sin(pitch * M_PI / 180.0f) * 16.0f;
    btScalar zf = playerPos.Z - (cos(yaw * M_PI / 180.0f) *
                                 cos(pitch * M_PI / 180.0f)) * 16.0f;
    camera->setPosition(irr::core::vector3df(xf, yf, zf));
    // Make the camera look at the vehicle
    camera->setTarget(playerPos);
    // Move the cursor back to (.5, .5)
    Game::device->getCursorControl()->setPosition(0.5f, 0.5f);
}
