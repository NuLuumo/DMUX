#pragma once

#include <BulletDynamics/Vehicle/btWheelInfo.h>
#include "comp/Graphics.hpp"
#include "comp/Physics.hpp"
class WorldData;

/**
 * \brief Tire (presumably part of TireSet in the vehicle class) on the vehicle
 * \details Part of the tireSet variable in the Vehicle class
 */
class Tire final {//, public btWheelInfo {
public:
    /**
     * \brief Default constructor
     * \param wd World that Tire is located in
     */
    Tire();

    void update() {} //!< Mandatory update function, called each frame
    /** Default destructor */
    virtual ~Tire();

private:
    gal::comp::Graphics graphics;
    gal::comp::Physics physics;
};
