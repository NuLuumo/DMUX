#include "Observer.hpp"
#include "Simulation.hpp"
#include "comp/Subject.hpp"

#include <UUID4.h>

namespace gal {

Observer::Observer() :
    ID(plt::GenUUID()) {
    puts(std::string("Observer created with ID: " + ID).c_str());
}

void Observer::discover(gal::comp::Subject &subject) {
    subject.kr.push_back(ID);
    subject.census[ID] = this;
}

Observer::~Observer() {
    //    Simulation::mir.subject.census.erase(ID);

    std::vector<std::string>::iterator iter = Simulation::mir.subject.kr.begin();

    while(iter != Simulation::mir.subject.kr.end()) {
        if(*iter == ID) {
            iter = Simulation::mir.subject.kr.erase(iter);
        } else {
            ++iter;
        }
    }

    puts(std::string("Observer destroyed with ID: " + ID).c_str());
}

}
