#pragma once

/**
 * @file   Game.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Draws debug information onto screen for debugging physics
 * this is an implementation of the Bullet Debug drawer for Irrlicht
 * a helpful person on the forums provided, however I have modified it
 * since then.
 *
 * Copyright (c) 2016 Collective Tyranny
 */

#include <irrlicht.h>
#include <btBulletDynamicsCommon.h>
#include <iostream>

/**
 * \brief Draws debug information onto screen for debugging physics
 */
class DebugDraw : public btIDebugDraw {

public:

    /**
     * \brief Constructor for Debug Draw
     * \details gets the video driver and logger, also sets the mode for debug draw
     */
    DebugDraw(irr::IrrlichtDevice *const device) :
        mode(DBG_NoDebug), driver(device->getVideoDriver()), logger(device->getLogger()) {

    }

    /**
     * \brief Draws a line in Irrlicht
     * \details There is some gnarly stuff since Irrlicht is pretty
     *  high level so this is nice.
     * \param from Origin of the line
     * \param to Destination of line
     * \param color Color of the line in RGB value, stored in vector
     */
    void drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color) {
        //workaround to bullet's inconsistent debug colors which are either from 0.0 - 1.0 or from 0.0 - 255.0
        irr::video::SColor newColor(255, (irr::u32)color[0], (irr::u32)color[1], (irr::u32)color[2]);
        if(color[0] <= 1.0 && color[0] > 0.0) {
            newColor.setRed((irr::u32)(color[0] * 255.0));
        }
        if(color[1] <= 1.0 && color[1] > 0.0) {
            newColor.setGreen((irr::u32)(color[1] * 255.0));
        }
        if(color[2] <= 1.0 && color[2] > 0.0) {
            newColor.setBlue((irr::u32)(color[2] * 255.0));
        }

        this->driver->draw3DLine(
            irr::core::vector3df(from[0], from[1], from[2]),
            irr::core::vector3df(to[0], to[1], to[2]),
            newColor);
    }

    /**
     * \brief Draws contact point when collision occurs
     * \param PointOnB Point of collision
     * \param normalOnB Normalized vector of collision point
     * \param distance distance between objects
     * \param lifeTime How long the collision occured
     * \param color RGB value for the color the point will be drawn
     */
    void drawContactPoint(const btVector3 &PointOnB, const btVector3 &normalOnB, btScalar distance, int lifeTime, const btVector3 &color) {
        static const irr::video::SColor CONTACTPOINT_COLOR(255, 255, 255, 0); //bullet's are black :(

        //   this->drawLine(PointOnB, PointOnB + normalOnB*distance, CONTACTPOINT_COLOR);

        const btVector3 to(PointOnB + normalOnB * distance);

        this->driver->draw3DLine(
            irr::core::vector3df(PointOnB[0], PointOnB[1], PointOnB[2]),
            irr::core::vector3df(to[0], to[1], to[2]),
            CONTACTPOINT_COLOR);
        lifeTime++;
        color.getX();
    }

    /**
     * \brief Reports if anything is going wrong
     * \param text Information you want Irrlicht to log
     */
    void reportErrorWarning(const char *text) {
        this->logger->log(text, irr::ELL_ERROR);
    }

    /**
     * \brief does nothing
     */
    void draw3dText(const btVector3 &location, const char *text) {
        if(text == nullptr) {
            location.getX();
        }

    }

    /**
     * \brief sets debug draw mode
     * \param mode Mode for debug draw
     */
    void setDebugMode(int mode) {
        this->mode = mode;
    }

    /**
     * \brief reports debug draw mode
     * \returns the debug draw mode
     */
    int getDebugMode() const {
        return this->mode;
    }

private:

    /**
     * \brief current state of debugdraw
     */
    int mode;

    /**
     * \brief pointer to video driver of our Irrlicht device
     */
    irr::video::IVideoDriver *const driver;

    /**
     * \brief To log things
     */
    irr::ILogger *logger;
};
