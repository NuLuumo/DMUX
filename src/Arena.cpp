#include "Arena.hpp"
#include "Simulation.hpp"
#include "client/JukeBox.hpp"
#include "sys/TriMesh.hpp"
#include "sys/Graphics.hpp"
#include "sys/Physics.hpp"

Arena::Arena() {

    graphics.objType = "tracks";
    graphics.objStr = "olivermath";

    gal::sys::addNode(graphics);

    physics.mesh = Simulation::device->getSceneManager()->getMesh(graphics.modelPath.c_str());
    physics.mass = btScalar(0.0f);
    physics.position = btVector3(0.0f, 0.0f, 0.0f);

    btBvhTriangleMeshShape *bvhShape = nullptr;
    physics.shape = gal::sys::buildCollisionShape(physics.mesh, bvhShape);
    physics.shape->setLocalScaling(btVector3(4.0f, 4.0f, 4.0f));
    physics.shape->setMargin(btScalar(0.07f));
    gal::sys::addToWorld(physics);
    // Store a pointer to the irrlicht node so we can update it later
    physics.rigidBody->setUserPointer(static_cast<void *>(graphics.node));
}
