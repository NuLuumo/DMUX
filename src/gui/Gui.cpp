#include "Gui.hpp"
#include "client/Game.hpp"
#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>

namespace gal {

Gui::Gui(IrrIMGUI::CIMGUIEventReceiver *i) :
    imguiSettings(new IrrIMGUI::SIMGUISettings()),
    fontDir(cpplocate::findModule("dmux").value("fontDir")),
    navWindowSize(std::make_pair(Game::playerSettings.currentWindowSize.first,
                                 Game::playerSettings.currentWindowSize.second / 10)),
    navWindowPos(std::make_pair(0, Game::playerSettings.currentWindowSize.second - (navWindowSize.second))),
    input(i),
    isClosed(false) {

    for(int i = 0; i < irr::KEY_KEY_CODES_COUNT; ++i) {
        keyIsDown[i] = false;
    }
}

void Gui::init() {

#ifndef NDEBUG
    assert(Game::device not_eq nullptr);
    assert(imguiSettings not_eq nullptr);
#endif

    //If the user specified their own event handler
    if(input not_eq nullptr) {
        pGUI = createIMGUI(Game::device, input, imguiSettings);
    } else {
        pGUI = createIMGUI(Game::device, this, imguiSettings);
    }

#ifndef NDEBUG
    assert(Game::device not_eq nullptr);
    assert(imguiSettings not_eq nullptr);
#endif

    // If the user set the cursor themselves
    if(imguiSettings->mIsGUIMouseCursorEnabled) {
        imguiSettings->mIsGUIMouseCursorEnabled = true;
    } else {
        imguiSettings->mIsGUIMouseCursorEnabled = false;
    } //default to no cursor

    Game::device->setEventReceiver(this);

    //Loading custom fonts for IrrIMGUI
    pSerifNav  = pGUI->addFontFromFileTTF(std::string(fontDir + "LiberationSerif-Bold.ttf").c_str(), (navWindowSize.second / 3));
    pSerifText = pGUI->addFontFromFileTTF(std::string(fontDir + "LiberationSerif-Regular.ttf").c_str(), (navWindowSize.second / 5));
    pMono      = pGUI->addFontFromFileTTF(std::string(fontDir + "LiberationMono-Regular.ttf").c_str(), (navWindowSize.second / 5));
    pGUI->compileFonts();

#ifndef NDEBUG
    assert(Game::device not_eq nullptr);
    assert(imguiSettings not_eq nullptr);
    assert(pGUI not_eq nullptr);
    assert(pSerifNav not_eq nullptr);
    assert(pSerifText not_eq nullptr);
    assert(pMono not_eq nullptr);
#endif


}

void Gui::closeMenu() {
    isClosed = true;
}
bool Gui::run() {
    return not isClosed;
}

bool Gui::isKeyDown(const irr::EKEY_CODE &keyCode) const {
    return keyIsDown[keyCode];
}

Gui::~Gui() {
    pGUI->drop();
    delete imguiSettings;
}

}
