#pragma once

/**
 * @file   GUI.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Abstract class that initialized IrrIMGUI and handles setup
 *
 * Copyright (c) 2016 Collective Tyranny
 */

#include <IrrIMGUI/IncludeIrrlicht.h>
#include <IrrIMGUI/IncludeIMGUI.h>
#include <IrrIMGUI/IrrIMGUI.h>
#include <string>

//macros for window attributs
#define BUTTON_WIDTH   (Game::playerSettings.currentWindowSize.first / 4)
#define WINDOW_WIDTH   (Game::playerSettings.currentWindowSize.first - (Game::playerSettings.currentWindowSize.first / 3))
#define WINDOW_HEIGHT  (Game::playerSettings.currentWindowSize.second - (Game::playerSettings.currentWindowSize.second / 5))
#define WINDOW_X_POS   ((Game::playerSettings.currentWindowSize.first - WINDOW_WIDTH) / 2)
#define WINDOW_Y_POS   ((Game::playerSettings.currentWindowSize.second - WINDOW_HEIGHT) / 2)

namespace gal {

class Gui : public IrrIMGUI::CIMGUIEventReceiver {

public:
    void closeMenu();
    bool run();

protected:
    Gui(IrrIMGUI::CIMGUIEventReceiver *i = nullptr);
    void init();
    virtual ~Gui();
    IrrIMGUI::SIMGUISettings *imguiSettings;
    IrrIMGUI::IIMGUIHandle *pGUI;
    std::string fontDir;
    ImFont *pSerifNav;
    ImFont *pSerifText;
    ImFont *pMono;
    std::pair <int, int> navWindowSize;
    std::pair <int, int> navWindowPos;
    bool keyIsDown[irr::KEY_KEY_CODES_COUNT];
    IrrIMGUI::CIMGUIEventReceiver *input;

private:
    bool isKeyDown(const irr::EKEY_CODE &keyCode) const;
    bool isClosed;
};

}
