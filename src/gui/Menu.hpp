#pragma once

/**
 * @file   MainMenuInput.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Handles user input for the main menu, self containing unit for the main menu
 *
 * Copyright (c) 2016 Collective Tyranny
 */

#include <vector>
#include <IrrIMGUI/IncludeIrrlicht.h>
#include <IrrIMGUI/IncludeIMGUI.h>
#include <IrrIMGUI/IrrIMGUI.h>
#include <fstream>
#include "Settings.hpp"
#include "Gui.hpp"

namespace menu {
class Menu final : public gal::Gui {
public:
    Menu();
    ~Menu();
    bool OnEvent(const irr::SEvent &event);
    void showMainMenu(); //!< Show main menu window with navbar
    bool showPauseMenu(); //!< returns whether or not to exit the pause menu
    bool showTeamSelection(); //!< returns the selected team, show the menu for team selection
private:
    void showNavBar();
    void showSettings();
    void showCredits();
    void videoSettingsInputMenu();
    void keyInputMenu();

    std::string logoDir;
    std::vector<irr::video::IImage *> teamLogos;
    std::vector<std::reference_wrapper<IrrIMGUI::IGUITexture>> logoTextures;
    std::vector<std::string> availableTeams;

    unsigned short selectedWindow;

    std::vector<std::string> availableButtons;
    std::vector<std::string> pauseMenuOptions;
    std::string contributorFilePath;
    std::ifstream contributorFileStream; //file
    std::string contributorFileLine; //str
    std::vector<std::string> contributorFileContent;
};
}
