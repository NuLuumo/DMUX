#pragma once

#include <irrlicht.h>
#include <vector>
#include <map>

extern std::map<const std::string, std::string> controls;
extern bool isKeyModifying;
extern std::string controlMod;

struct Settings final {
public:
    Settings();
    irr::SIrrlichtCreationParameters getIrrlichtParams(irr::IEventReceiver *rec = nullptr);
    std::vector<std::pair<std::string, int>> volumes;
    std::vector<std::pair<std::string, bool>> toggle;
    std::map<const std::string, std::string> controls;
    std::vector<std::pair<int, int>> supportedResolutions;
    std::pair <int, int> currentWindowSize;
    irr::f32 mouseSensitivity;
    Settings &operator = (Settings &arg);
    int selectedResolution;
    void saveSettingsXML();
    irr::f32 getMasterVolume();
    irr::f32 getSoundVolume();
    irr::f32 getMusicVolume();
    char alias[32];
private:
    std::string fileName;
    void readXML();
    void *operator new(size_t);     //!< Preventing heap allocation
    void *operator new[](size_t);   //!< Preventing heap allocation
    void  operator delete(void *) {}    //!< Preventing heap allocation
    void  operator delete[](void *) {} //!< Preventing heap allocation
};
