#include <iostream>
#include <map>
#include <vector>
#include <fstream>
#include <IrrIMGUI/IncludeIrrlicht.h>
#include <IrrIMGUI/IncludeIMGUI.h>
#include <IrrIMGUI/IrrIMGUI.h>
#include <IrrIMGUI/IrrIMGUIDebug.h>
#include <tinyxml2.h>
#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>

#include "client/Game.hpp"
#include "client/JukeBox.hpp"
#include "Settings.hpp"

// TODO (bkeys#1#): Reorganize buttons to not be in alphabetical order ...
//Make the characters in the menu not be shown as numbers (they need to stay as numbers under the hood)

inline bool fileExists(const char *name);

//menu::EventListener EventReceiver;
std::string controlMod;

inline bool fileExists(const char *name) {
    std::ifstream f(name);
    return f.good();
}

Settings::Settings() {

    fileName = cpplocate::findModule("dmux").value("projectDir") + std::string("save.xml");

    //defaults . . . will get erased if settings file exists
    controls["Forward"] = "w";
    controls["Backward"] = "s";
    controls["Right"] = "d";
    controls["Left"] = "a";
    controls["Jump"] = "j";
    controls["Crouch"] = "c";
    controls["Sprint"] = "q" ;

    controls["Attack"] = "e";
    controls["Alt-Attack"] = "t";
    controls["Reload"] = "r";
    controls["Aim"] = "y";

    volumes.push_back(std::make_pair("Master", 0));
    volumes.push_back(std::make_pair("Music", 0));
    volumes.push_back(std::make_pair("SoundFX", 0));

    toggle.push_back(std::make_pair("FullScreen", false));
    toggle.push_back(std::make_pair("v-sync", false));
    toggle.push_back(std::make_pair("Anti-Aliasing", false));

    supportedResolutions.push_back(std::make_pair(800, 600));
    supportedResolutions.push_back(std::make_pair(1024, 768));
    supportedResolutions.push_back(std::make_pair(1280, 800));
    supportedResolutions.push_back(std::make_pair(1366, 768));
    supportedResolutions.push_back(std::make_pair(1280, 1024));
    supportedResolutions.push_back(std::make_pair(1440, 900));
    supportedResolutions.push_back(std::make_pair(1680, 1050));
    supportedResolutions.push_back(std::make_pair(1920, 1080));
    supportedResolutions.push_back(std::make_pair(1600, 900));
    supportedResolutions.push_back(std::make_pair(320, 480));

    currentWindowSize.first  = 800;
    currentWindowSize.second = 600;

    //Overwrite these settings if we have a save file
    if(fileExists(fileName.c_str())) {
        readXML();
    }

    for(unsigned int i = 0; i < supportedResolutions.size(); ++i) {
        if(supportedResolutions[i] == currentWindowSize) {
            selectedResolution = i;
        }
    }
}

irr::f32 Settings::getMasterVolume() {
    for(const auto &volume : volumes) {
        if(volume.first == std::string("Master")) {
            return static_cast<irr::f32>(volume.second);
        }
    }
    return 0;
}

irr::f32 Settings::getMusicVolume() {
    for(const auto &volume : volumes) {
        if(volume.first == std::string("Music")) {
            return static_cast<irr::f32>(volume.second);
        }
    }
    return 0;
}

irr::f32 Settings::getSoundVolume() {
    for(const auto &volume : volumes) {
        if(volume.first == std::string("SoundFX")) {
            return static_cast<irr::f32>(volume.second);
        }
    }
    return 0;
}

void Settings::saveSettingsXML() {

    tinyxml2::XMLDocument xmlDoc;
    tinyxml2::XMLNode *pRoot = xmlDoc.NewElement("Settings");
    xmlDoc.InsertFirstChild(pRoot);

    for(auto const &volume : Game::playerSettings.volumes) {
        tinyxml2::XMLElement *volumeTag = xmlDoc.NewElement(std::string(volume.first + std::string("Volume")).c_str());
        volumeTag->SetAttribute("Level", std::to_string(volume.second).c_str());
        pRoot->InsertEndChild(volumeTag);
    }

    for(auto const &box : Game::playerSettings.toggle) {
        tinyxml2::XMLElement *boxTag = xmlDoc.NewElement(box.first.c_str());
        if(box.second) {
            boxTag->SetAttribute("State", "On");
        } else {
            boxTag->SetAttribute("State", "Off");
        }
        pRoot->InsertEndChild(boxTag);
    }

    for(auto const &name : Game::playerSettings.controls) {
        tinyxml2::XMLElement *pElement = xmlDoc.NewElement("Control");
        pElement->SetAttribute("Action", name.first.c_str());
        pElement->SetAttribute("Value", name.second.c_str());
        pRoot->InsertEndChild(pElement);
    }

    {
        std::string xValue = std::to_string(Game::playerSettings.supportedResolutions[Game::playerSettings.selectedResolution].first);
        std::string yValue = std::to_string(Game::playerSettings.supportedResolutions[Game::playerSettings.selectedResolution].second);
        tinyxml2::XMLElement *pElement = xmlDoc.NewElement("Resolution");
        pElement->SetAttribute("xValue", xValue.c_str());
        pElement->SetAttribute("yValue", yValue.c_str());
        pRoot->InsertEndChild(pElement);
    }

    {
        tinyxml2::XMLElement *pElement = xmlDoc.NewElement("MouseSens");
        pElement->SetAttribute("Value", std::to_string(Game::playerSettings.mouseSensitivity).c_str());
        pRoot->InsertEndChild(pElement);
    }

    {
        tinyxml2::XMLElement *pElement = xmlDoc.NewElement("Alias");
        pElement->SetAttribute("Value", Game::playerSettings.alias);
        pRoot->InsertEndChild(pElement);
    }


    xmlDoc.SaveFile(fileName.c_str());

    std::cout << "Settings saved successfully" << std::endl;
}

irr::SIrrlichtCreationParameters Settings::getIrrlichtParams(irr::IEventReceiver *rec) {

    bool vSync = false;
    bool antiAlias = false;
    bool fullScreen = false;

    for(auto &option : toggle) {
        if(std::string("v-sync") == option.first) {
            vSync = option.second;
        }

        if(std::string("Anti-Aliasing") == option.first) {
            antiAlias = option.second;
        }

        if(std::string("FullScreen") == option.first) {
            fullScreen = option.second;
        }
    }

    irr::SIrrlichtCreationParameters IrrlichtParams;
    IrrlichtParams.DriverType    = irr::video::EDT_OPENGL;
    IrrlichtParams.WindowSize    = irr::core::dimension2d<irr::u32>(currentWindowSize.first, currentWindowSize.second);
    IrrlichtParams.Bits          = 32;
    IrrlichtParams.Fullscreen    = fullScreen;
    IrrlichtParams.Stencilbuffer = true;

    if(antiAlias) {
        IrrlichtParams.AntiAlias     = 16;
    } else {
        IrrlichtParams.AntiAlias     = 0;
    }

    IrrlichtParams.Vsync         = vSync;
    IrrlichtParams.EventReceiver = rec;

    return IrrlichtParams;
}
void Settings::readXML() {

    if(fileExists(fileName.c_str())) {
        tinyxml2::XMLDocument doc;

        try {
            tinyxml2::XMLError eResult =
                doc.LoadFile(fileName.c_str());
            if(eResult not_eq tinyxml2::XML_SUCCESS) {
                throw std::string("There is something seriously wrong with your installation of TinyXML2\n");
            }
        } catch(const std::string e) {
            doc.PrintError();
            std::cerr << e << std::endl;
            return;
        }

        for(auto &volume : volumes) {
            tinyxml2::XMLElement *volumeTag =
                doc.FirstChildElement("Settings")->FirstChildElement(std::string(volume.first.c_str() + std::string("Volume")).c_str());
            volume.second = std::stoi(std::string(volumeTag->Attribute("Level")));
        }

        for(auto &box : toggle) {
            tinyxml2::XMLElement *boxTag =
                doc.FirstChildElement("Settings")->FirstChildElement(box.first.c_str());
            if(std::string(boxTag->Attribute("State")) == std::string("On")) {
                box.second = true;
            } else {
                box.second = false;
            }
        }

        tinyxml2::XMLElement *root = doc.FirstChildElement("Settings");

        for(tinyxml2::XMLElement *e = root->FirstChildElement("Control"); e != nullptr;
                e = e->NextSiblingElement("Control")) {
            controls[std::string(e->Attribute("Action"))] =  e->Attribute("Value");
        }

        mouseSensitivity = std::stoi(std::string(root->FirstChildElement("MouseSens")->Attribute("Value")));
        strcpy(alias, root->FirstChildElement("Alias")->Attribute("Value"));

        selectedResolution = [this] {

            tinyxml2::XMLDocument doc;
            doc.LoadFile(fileName.c_str());

            tinyxml2::XMLElement *root =
            doc.FirstChildElement("Settings");

            currentWindowSize.first  = std::stoi(std::string(root->FirstChildElement("Resolution")->Attribute("xValue")));
            currentWindowSize.second = std::stoi(std::string(root->FirstChildElement("Resolution")->Attribute("yValue")));

            for(unsigned int i = 0; i < supportedResolutions.size(); ++i) {
                if(supportedResolutions[i].first == currentWindowSize.first and
                supportedResolutions[i].second == currentWindowSize.second) {
                    return i;
                }
            }
            return static_cast<unsigned int>(0);
        }();

        std::cout << "Read XML successfully\n";

    } else {
        std::cout << "settings file does not exist, making a new one" << std::endl;
    }
}

Settings &Settings::operator = (Settings &arg) {
    volumes = arg.volumes;
    toggle = arg.toggle;
    controls = arg.controls;
    supportedResolutions = arg.supportedResolutions;
    mouseSensitivity = arg.mouseSensitivity;
    return arg;
}
