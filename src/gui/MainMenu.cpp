#include <iostream>
#include "MainMenu.hpp"
#include "client/Game.hpp"
#include "client/JukeBox.hpp"
#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>
#include "client/Game.hpp"

//CONTROLS
bool isKeyInputMenuActive = false;
bool isVideoSettingsMenuActive = false;
bool isKeyModifying = false;

namespace menu {
Menu::Menu() :
    Gui(),
    logoDir(cpplocate::findModule("dmux").value("logoDir")),
    selectedWindow(10), //we want no window selected
    contributorFilePath(cpplocate::findModule("dmux").value("projectDir") + std::string("CONTRIBUTORS.md")),
    contributorFileStream(contributorFilePath) {

    //Creating menu buttons for main menu
    availableButtons.push_back("Find Game");
    availableButtons.push_back("Settings");
    availableButtons.push_back("Credits");
    availableButtons.push_back("Exit");

    //options for pause menu
    pauseMenuOptions.push_back("Return to game");
    pauseMenuOptions.push_back("Change Team Alignment");
    pauseMenuOptions.push_back("Settings");
    pauseMenuOptions.push_back("Leave game");
    pauseMenuOptions.push_back("Exit");

#ifndef NDEBUG
    assert(Game::device not_eq nullptr);
#endif
    init();
#ifndef NDEBUG
    assert(imguiSettings not_eq nullptr);
    assert(pGUI not_eq nullptr);
#endif

    isKeyModifying = false;

    //Getting content of contributors file for credit slide
    while(std::getline(contributorFileStream, contributorFileLine)) {
        contributorFileContent.push_back(contributorFileLine);
    }

    //settings available teams, eventually we will
    //get these in the form of a packet from the server
    availableTeams.push_back("Fascist");
    availableTeams.push_back("Communist");
    availableTeams.push_back("Feminist");

    for(const auto &teamName : availableTeams) {
        teamLogos.push_back(Game::device->getVideoDriver()->createImageFromFile(std::string(logoDir + teamName + ".png").c_str()));
    }
    for(const auto &logo : teamLogos) {
        logoTextures.push_back(*pGUI->createTexture(logo));
        logo->drop();
    }
}

Menu::~Menu() {
    for(const auto &logo : logoTextures) {
        pGUI->deleteTexture(&logo.get());
    }
}

bool Menu::showTeamSelection() {

    ImGui::SetNextWindowPos(ImVec2(WINDOW_X_POS, WINDOW_Y_POS));
    ImGui::SetNextWindowSize(ImVec2(WINDOW_WIDTH, WINDOW_HEIGHT / 2));

    ImGui::Begin("Team Selection", nullptr, ImGuiWindowFlags_ShowBorders);

    ImGui::BeginGroup();
    ImGui::Text("Available Teams");
    for(const auto &logo : logoTextures) {
        ImGui::Image(logo.get(), ImVec2((WINDOW_WIDTH / 3) - 10, WINDOW_HEIGHT / 3));
        ImGui::SameLine();
    }
    ImGui::EndGroup();

    ImGui::BeginGroup();
    ImGui::PushFont(pSerifNav);
    for(const auto &team : availableTeams) {
        if(ImGui::Button(team.c_str(), ImVec2((WINDOW_WIDTH / 3) - 10, 50.0f))) {
            JukeBox::playFX("metal_interactions/metal_interaction2.ogg");
            ImGui::PopFont();
            ImGui::EndGroup();
            ImGui::End();
            Game::device->getCursorControl()->setVisible(false); // in case it is the first selection

            return true; //team has been selected
        }
        ImGui::SameLine();
    }
    ImGui::PopFont();
    ImGui::EndGroup();

    ImGui::End();
    return false; //team has not been selected
}

bool Menu::showPauseMenu() {

    ImGui::SetNextWindowPos(ImVec2(WINDOW_X_POS, WINDOW_Y_POS));
    ImGui::SetNextWindowSize(ImVec2(WINDOW_WIDTH, WINDOW_HEIGHT));

    ImGui::Begin("Pause Menu", nullptr, ImGuiWindowFlags_NoTitleBar);
    for(const auto &text : pauseMenuOptions) {
        if(ImGui::Button(text.c_str(), ImVec2(200, 40))) {
            if(text == std::string("Return to game")) {
                Game::device->getCursorControl()->setVisible(false);
                ImGui::End();
                return false; //no longer in pause menu
            } else if(text == std::string("Change Team Alignment")) {
                selectedWindow = 1;
            } else if(text == std::string("Settings")) {
                selectedWindow = 2;
            } else if(text == std::string("Leave game")) {

            } else if(text == std::string("Exit")) {
                Game::device->closeDevice();
            }
        }
    }
    ImGui::End();

    switch(selectedWindow) {
        case 0:
            closeMenu();
            break;
        case 1:
            Game::device->getCursorControl()->setVisible(true);
            if(showTeamSelection()) {
                selectedWindow = 10; //no selected window
            }
            break;
        case 2:
            showSettings();
            break;
        case 3:
            Game::device->closeDevice();
            break;
    }
    return true; //still in menu
}


void Menu::showMainMenu() {
    pGUI->startGUI();

    showNavBar();
    switch(selectedWindow) {
        case 0:
            closeMenu();
            break;
        case 1:
            showSettings();
            break;
        case 2:
            showCredits();
            break;
        case 3:
            Game::device->closeDevice();
            break;
    }

    Game::device->getSceneManager()->drawAll();
    pGUI->drawAll();
}

void Menu::showNavBar() {

    ImGui::SetNextWindowPos(ImVec2(navWindowPos.first, navWindowPos.second));
    ImGui::SetNextWindowSize(ImVec2(navWindowSize.first, navWindowSize.second));

    ImGui::Begin("Main Menu", NULL, ImGuiWindowFlags_NoTitleBar);
    ImGui::BeginGroup();
    ImGui::PushFont(pSerifNav);
    for(unsigned int i = 0; i < availableButtons.size(); ++i) {
        if(ImGui::Button(availableButtons[i].c_str(), ImVec2(navWindowSize.first / 4,
                         navWindowSize.second - 30))) {
            JukeBox::playFX("metal_interactions/metal_interaction1.ogg");
            selectedWindow = i;
        }
        ImGui::SameLine();
    }
    ImGui::PopFont();
    ImGui::EndGroup();
    ImGui::End();

}

bool Menu::OnEvent(const irr::SEvent &event) {
    GuiHandle(event);
    switch(event.EventType) {
        case irr::EET_KEY_INPUT_EVENT:
            if(isKeyModifying) {
                Game::playerSettings.controls[controlMod] = std::to_string(event.KeyInput.Key);
                isKeyModifying = false;
            }
            keyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
            break;
        case irr::EET_LOG_TEXT_EVENT:
            //            logMessage(event.LogEvent.Text);
            return true;
        default:
            break;
    }
    return false;
}

void modKey(const char *label);
void keyInputMenu();
void saveSettingsXML();

void modKey(const char *label) {
    if(isKeyModifying) {
        ImGui::Text(std::string("Press key for ").append(label).c_str());
    }
}

void Menu::videoSettingsInputMenu() {

    if(isVideoSettingsMenuActive) {
        ImGui::PushFont(pSerifText);
        ImGui::Begin("Video Settings", nullptr, ImGuiWindowFlags_ShowBorders);
        ImGui::SameLine(0.0f, 10.0f);
        ImGui::BeginGroup();

        for(auto &box : Game::playerSettings.toggle) {
            if(ImGui::Checkbox(box.first.c_str(), &box.second)) {
                JukeBox::playFX("metal_interactions/metal_button_press1.ogg");
            }
        }

        ImGui::Text("Resolutions");

        for(unsigned int i = 0; i < Game::playerSettings.supportedResolutions.size(); ++i) {
            std::string text = (std::to_string(Game::playerSettings.supportedResolutions[i].first) + " x " + std::to_string(Game::playerSettings.supportedResolutions[i].second));

            if((Game::playerSettings.supportedResolutions[i].first == Game::playerSettings.supportedResolutions[Game::playerSettings.selectedResolution].first) and
                    (Game::playerSettings.supportedResolutions[i].second == Game::playerSettings.supportedResolutions[Game::playerSettings.selectedResolution].second)) {
                if(ImGui::RadioButton(text.c_str(), true)) {
                    JukeBox::playFX("metal_interactions/metal_button_press2.ogg");
                    Game::playerSettings.selectedResolution = i;
                }
            } else {
                if(ImGui::RadioButton(text.c_str(), false)) {
                    JukeBox::playFX("metal_interactions/metal_button_press2.ogg");
                    Game::playerSettings.selectedResolution = i;
                }
            }
        }

        if(ImGui::Button("Okay", ImVec2(BUTTON_WIDTH, 30))) {
            JukeBox::playFX("metal_interactions/metal_interaction1.ogg");
            isVideoSettingsMenuActive = false;
        }

        ImGui::EndGroup();
        ImGui::End();
        ImGui::PopFont();
    }
}

void Menu::keyInputMenu() {

    if(isKeyInputMenuActive) {
        ImGui::PushFont(pSerifText);
        ImGui::Begin("Controls", nullptr, ImGuiWindowFlags_ShowBorders);
        ImGui::SameLine(0.0f, 10.0f);
        ImGui::BeginGroup();
        if(ImGui::SliderFloat("Mouse sensitivity", &Game::playerSettings.mouseSensitivity, irr::u32(1), irr::u32(100))) {
            JukeBox::playFX("metal_interactions/metal_swing1.ogg");
            Game::playerSettings.saveSettingsXML();
        }

        for(auto const &name : Game::playerSettings.controls) {
            ImGui::BeginGroup();
            if(ImGui::Button(name.first.c_str(), ImVec2(BUTTON_WIDTH, 30))) {
                JukeBox::playFX("metal_interactions/metal_button_press2.ogg");
                isKeyModifying = true;
                controlMod = name.first;
                Game::playerSettings.saveSettingsXML();
            }
            ImGui::SameLine();
            ImGui::Text(name.second.c_str());
            ImGui::EndGroup();
        }

        if(ImGui::Button("Okay", ImVec2(BUTTON_WIDTH, 30))) {
            JukeBox::playFX("metal_interactions/metal_interaction1.ogg");
            isKeyInputMenuActive = false;
        }

        //If the user is modifying a key input
        modKey(controlMod.c_str());
        ImGui::EndGroup();
        ImGui::End();
        ImGui::PopFont();
    }
}

void Menu::showSettings() {

    // create second window with some control inputs
    ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f));
    ImGui::SetNextWindowSize(ImVec2(Game::playerSettings.currentWindowSize.first,
                                    Game::playerSettings.currentWindowSize.second - (Game::playerSettings.currentWindowSize.second / 9)));

    ImGui::PushFont(pSerifText);
    ImGui::Begin("Player Settings", NULL, ImGuiWindowFlags_NoTitleBar);
    ImGui::BeginGroup();
    ImGui::EndGroup();
    ImGui::SameLine(0.0f, 10.0f);
    ImGui::BeginGroup();

    for(auto &volume : Game::playerSettings.volumes) {
        if(ImGui::SliderInt(std::string(volume.first + std::string(" Volume")).c_str(), &volume.second, irr::u32(0), irr::u32(100))) {
            JukeBox::playFX("metal_interactions/metal_swing1.ogg");
            Game::playerSettings.saveSettingsXML();
        }
    }

    if(ImGui::Button("Controls", ImVec2(BUTTON_WIDTH, 30))) {
        JukeBox::playFX("metal_interactions/metal_interaction2.ogg");
        if(not isKeyInputMenuActive) {
            ImGui::SetNextWindowPos(ImVec2(WINDOW_X_POS, WINDOW_Y_POS));
            ImGui::SetNextWindowSize(ImVec2(WINDOW_WIDTH, WINDOW_HEIGHT));
        }
        isKeyInputMenuActive = true;
    }

    if(ImGui::Button("Video Settings", ImVec2(BUTTON_WIDTH, 30))) {
        JukeBox::playFX("metal_interactions/metal_interaction2.ogg");
        ImGui::SetNextWindowPos(ImVec2(WINDOW_X_POS, WINDOW_Y_POS));
        ImGui::SetNextWindowSize(ImVec2(WINDOW_WIDTH, WINDOW_HEIGHT));
        isVideoSettingsMenuActive = true;
    }

    if(ImGui::Button("Apply", ImVec2(BUTTON_WIDTH, 20))) {
        JukeBox::playFX("metal_interactions/metal_interaction2.ogg");
        Game::playerSettings.saveSettingsXML();
    }

    if(ImGui::Button("Okay", ImVec2(BUTTON_WIDTH, 20))) {
        JukeBox::playFX("metal_interactions/metal_interaction2.ogg");
        selectedWindow = 10; //make the settings window inactive
    }

    videoSettingsInputMenu();
    keyInputMenu();
    ImGui::EndGroup();
    ImGui::End();
    ImGui::PopFont();
}

void Menu::showCredits() {

    ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f));
    ImGui::SetNextWindowSize(ImVec2(Game::playerSettings.currentWindowSize.first,
                                    Game::playerSettings.currentWindowSize.second - (Game::playerSettings.currentWindowSize.second / 9)));

    ImGui::PushFont(pMono);
    ImGui::Begin("Credits", NULL, ImGuiWindowFlags_ShowBorders);
    ImGui::BeginGroup();

    for(const auto &text : contributorFileContent) {
        ImGui::Text(text.c_str());
    }

    ImGui::EndGroup();
    ImGui::SameLine(0.0f, 10.0f);
    ImGui::End();
    ImGui::PopFont();
}
}
