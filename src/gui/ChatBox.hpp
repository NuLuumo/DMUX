#pragma once

/**
 * @file   ChatBox.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Chat box on hud for in game chat
 *
 * Copyright (c) 2016 Collective Tyranny
 */

#include <string>
#include "gui/Gui.hpp"

class ChatBox final : public gal::Gui {

public:
    ChatBox();
    void show();
    bool OnEvent(const irr::SEvent &event);
    ~ChatBox();

private:
    std::string chatText;
};
