#include "Tire.hpp"
#include "sys/TriMesh.hpp"
#include "sys/Graphics.hpp"
#include "sys/Physics.hpp"
#include "Simulation.hpp"

Tire::Tire() {

    graphics.objType = "tire";
    graphics.objStr  = "Continent0";
    gal::sys::addNode(graphics);

    physics.mesh = Simulation::device->getSceneManager()->getMesh(graphics.modelPath.c_str());
    physics.mass = btScalar(20.0f);
    physics.shape = gal::sys::buildConvexHullShape(physics.mesh);
    physics.position = btVector3(0.0f, 10.0f, 0.0f);
    gal::sys::addToWorld(physics);
    physics.rigidBody->setUserPointer(static_cast<void *>(graphics.node));
}

Tire::~Tire() {
}
