#pragma once

/**
 * @file   Logger.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Abstract logging mechanism
 * Copyright (c) 2016 Collective Tyranny
 */

#include <fstream>
#include <chrono>
#include <ctime>
#include <string>

class Logger {

protected:
    Logger(const char *lName);
    virtual ~Logger();
    void logMessage(const char *logMessage); //!< Writes message to log file
    std::chrono::time_point<std::chrono::system_clock> start; //!< Start of application running
    std::chrono::time_point<std::chrono::system_clock> end; //!< End of application running
    std::ofstream fileLogger; //!< File stream that handles file I/O for our logger
private:
    std::string logName;

protected:
    std::string logFileName; //!< name of file we put the log info into

private:
    void *operator new(size_t);     //!< Preventing heap allocation
    void *operator new[](size_t);   //!< Preventing heap allocation
    void  operator delete[](void *) {} //!< Preventing heap allocation
    static std::string logDir;
};
