#include "PacketTypes.hpp"
#include <cstring>

/**
 * TODO: anything having to do with endianness and 32-bit vs 64-bit variables
 * should be solved in these packet creating (and the counter-part package
 * reading) functions so as to centralize and make things easier to maintain.
 * Besides, both server and client will have to deal with these problems.
 */
char *createActionKeysPacket(bool actionKeysDown[NUM_ACTION_KEYS]) {
    ActionKeysPacket packetStruct;
    packetStruct.typeId = ACTIONS_KEYS_PRESSED;
    for(int i = 0; i < NUM_ACTION_KEYS; ++i) {
        packetStruct.actionKeys[i] = actionKeysDown[i];
    }
    char *packet = NULL;
    std::memcpy(packet, &packetStruct, sizeof(packetStruct));
    return packet;
}

char *createGunRotationPacket(irr::f32 x, irr::f32 y, irr::f32 z) {
    GunRotationPacket packetStruct;
    packetStruct.typeId = GUN_ROTATION;
    packetStruct.x = x;
    packetStruct.y = y;
    packetStruct.z = z;
    char *packet = NULL;
    std::memcpy(packet, &packetStruct, sizeof(packetStruct));
    return packet;
}
