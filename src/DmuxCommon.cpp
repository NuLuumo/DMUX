#include <iostream>
#include <cassert>
#include <sstream>
#include <fstream>
#include "DmuxCommon.hpp"

btVector3 irrToBtVector(const irr::core::vector3df &vec) {
    return btVector3(vec.X, vec.Y, vec.Z);
}

irr::core::vector3df btVectorToIrr(const btVector3 &vec) {
    return irr::core::vector3df(vec.getX(), vec.getY(), vec.getZ());
}

irr::core::vector3df stringToIrrVector(const std::string &v) {
#ifndef NDEBUG
    assert(not v.empty());
#endif
    std::istringstream iss {v};
    irr::f32 x, y, z;
    iss >> x >> y >> z;
    return irr::core::vector3df(x, y, z);
}

void getMeshSize(const irr::scene::IAnimatedMeshSceneNode *mesh) {
#ifndef NDEBUG
    assert(mesh not_eq nullptr);
#endif

    irr::core::vector3d<irr::f32> *edges = new irr::core::vector3d<irr::f32>[8];
    irr::core::aabbox3d<irr::f32> boundingbox = mesh->getTransformedBoundingBox();
    boundingbox.getEdges(edges);

    irr::f32 height = edges[1].Y - edges[0].Y; //OK
    std::cout << "height: " << height << std::endl;

    irr::f32 width = edges[5].X - edges[1].X;
    std::cout << "width: " << width << std::endl;

    irr::f32 depth = edges[2].Z - edges[0].Z;
    std::cout << "depth: " << depth << std::endl;
}

bool fileExists(const char *name) {
    std::ifstream f(name);
    return f.good();
}
