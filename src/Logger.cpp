#include "Logger.hpp"
#include "DmuxCommon.hpp"

#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>
#include <iostream>

#ifndef NDEBUG
#include <cassert>
#endif

std::string Logger::logDir;

Logger::Logger(const char *lName) :
    start(std::chrono::system_clock::now()),
    logName(lName),
    logFileName(std::string("dmux_") + logName + "Logs.txt") {
    if(logDir.empty()) {
        logDir = cpplocate::findModule("dmux").value("logDir");
    }

    std::chrono::system_clock::time_point today = start;
    std::time_t tt = std::chrono::system_clock::to_time_t (today);

    if(fileExists(logFileName.c_str())) {
        fileLogger = std::ofstream(logDir + logFileName, std::ios::app);
    } else {
        fileLogger = std::ofstream(logDir + logFileName);
    }

    fileLogger << "Irrlicht began: " << ctime(&tt);
    fileLogger.close();
}

void Logger::logMessage(const char *logMessage) {

    fileLogger = std::ofstream(logDir + logFileName, std::ios::app);
    fileLogger << logMessage << std::endl;
#ifndef NDEBUG
    assert(fileLogger.good());
#endif
    fileLogger.close();

    std::cout << logMessage << std::endl;
}

Logger::~Logger() {

    fileLogger = std::ofstream(logDir + logFileName, std::ios::app);
    end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = end - start;
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);

    fileLogger << logName << " ended: " << std::ctime(&end_time) << std::endl;
    fileLogger.close();
}
