#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>
#ifndef NDEBUG
#include <cassert>
#include <iostream>
#endif

#include "Vehicle.hpp"
#include "Simulation.hpp"
#include "sys/TriMesh.hpp"
#include "sys/Graphics.hpp"
#include "sys/Physics.hpp"

std::string Vehicle::chassisDir = cpplocate::findModule("dmux").value("chassisDir");
std::string Vehicle::tireDir = cpplocate::findModule("dmux").value("tireDir");

void Vehicle::parseSceneXML() {
}

irr::core::vector3df Vehicle::getXMLWheelPos(const char *a) {
    tinyxml2::XMLDocument doc;
    initXMLDoc(doc);
    tinyxml2::XMLElement *wheelsTag =
        doc.FirstChildElement("kart")->FirstChildElement("wheels");
#ifndef NDEBUG
    assert(wheelsTag not_eq nullptr);
#endif
    return stringToIrrVector(std::string(wheelsTag->FirstChildElement(
            a)->Attribute("position")));
}

void Vehicle::initXMLDoc(tinyxml2::XMLDocument &doc) {


    try {
        tinyxml2::XMLError eResult =
            doc.LoadFile(std::string(chassisDir + graphics.objStr + std::string("/kart.xml")).c_str());

        if(eResult not_eq tinyxml2::XML_SUCCESS) {
            throw std::string("Error: XML file does not exist or is corrupt (like Hillary Clinton)");
        }
    } catch(const std::string e) {
        doc.PrintError();
        std::cerr << e << std::endl;
        std::cerr <<
                  "DMUX is going to have to make an educated guess at where your tires will connect, where the vehicle's weapon gets mounted, etc. This error WILL NOT be solved by restarting the game."
                  << std::endl <<
                  "If you can replicate this error please email the DMUX developers at bkeys@gnu.org with the error message, your operating system, and version of DMUX you have installed"
                  << std::endl;
        return;
    }
}

std::string Vehicle::getXMLKartAttribute(const char *attr) {
    tinyxml2::XMLDocument doc;
    initXMLDoc(doc);

    tinyxml2::XMLElement *kartTag = doc.FirstChildElement("kart");
#ifndef NDEBUG
    assert(kartTag not_eq nullptr);
#endif
    return std::string(kartTag->Attribute(attr));
}

std::string Vehicle::getXMLEngineSound() {
    tinyxml2::XMLDocument doc;
    initXMLDoc(doc);
    tinyxml2::XMLElement *soundTag =
        doc.FirstChildElement("kart")->FirstChildElement("sounds");
#ifndef NDEBUG
    assert(soundTag not_eq nullptr);
#endif
    return std::string(soundTag->Attribute("engine"));
}

irr::core::vector3df Vehicle::getXMLWeaponMount() {
    tinyxml2::XMLDocument doc;
    initXMLDoc(doc);
    tinyxml2::XMLElement *weaponTag =
        doc.FirstChildElement("kart")->FirstChildElement("speed-weighted-objects")->FirstChildElement("speed-weighted");
    return stringToIrrVector(std::string(
                                 weaponTag->Attribute("position")));
}

Vehicle::Vehicle(const char *nodePath,
                 const irr::core::vector3df &pos) :
    Observer(),
    wheelDirectionCS0(0, -1, 0),
    wheelAxleCS(-1, 0, 0),
    suspensionRestLength(0.5),
    wheelWidth(0.8),
    wheelRadius(0.5f),
    connectionHeight(1.0),
    steer(0),
    t() {

    graphics.objType = "chassis";
    graphics.objStr  = nodePath;

    name = getXMLKartAttribute("name");
    version = getXMLKartAttribute("version");
    iconFile = getXMLKartAttribute("icon-file");
    type = getXMLKartAttribute("type");
    groups = getXMLKartAttribute("groups");
    engineSound = getXMLEngineSound();
    weaponMountPoint = getXMLWeaponMount();
    rearRightWheelConnection = getXMLWheelPos("rear-right");
    rearLeftWheelConnection = getXMLWheelPos("rear-left");
    frontLeftWheelConnection = getXMLWheelPos("front-left");
    frontRightWheelConnection = getXMLWheelPos("front-right");

    gal::sys::addNode(graphics);

    //    parseKartXML();
    std::string wheelName = tireDir + "Continent0/Continent0.obj";

    rearRightWheel = Simulation::device->getSceneManager()->addAnimatedMeshSceneNode(
                         Simulation::device->getSceneManager()->getMesh(wheelName.c_str()));

    rearRightWheel->setParent(graphics.node);
    rearRightWheel->setPosition(rearRightWheelConnection);

    rearLeftWheel = Simulation::device->getSceneManager()->addAnimatedMeshSceneNode(
                        Simulation::device->getSceneManager()->getMesh(wheelName.c_str()));
    rearLeftWheel->setParent(graphics.node);
    rearLeftWheel->setPosition(rearLeftWheelConnection);

    frontLeftWheel = Simulation::device->getSceneManager()->addAnimatedMeshSceneNode(
                         Simulation::device->getSceneManager()->getMesh(wheelName.c_str()));
    frontLeftWheel->setParent(graphics.node);
    frontLeftWheel->setPosition(frontLeftWheelConnection);

    frontRightWheel = Simulation::device->getSceneManager()->addAnimatedMeshSceneNode(
                          Simulation::device->getSceneManager()->getMesh(wheelName.c_str()));
    frontRightWheel->setParent(graphics.node);
    frontRightWheel->setPosition(frontRightWheelConnection);

    //Never deactivate the vehicle

    physics.position = btVector3(pos.X, pos.Y, pos.Z);
    physics.mass = btScalar(1800.0f);
    physics.mesh = Simulation::device->getSceneManager()->getMesh(graphics.modelPath.c_str());
    physics.shape = gal::sys::buildConvexHullShape(physics.mesh);
    gal::sys::addToWorld(physics);
    physics.rigidBody->setUserPointer(static_cast<void *>(graphics.node));

    discover(Simulation::mir.subject);
    physics.rigidBody->setActivationState(DISABLE_DEACTIVATION);
    physics.rigidBody->setDamping(0.2, 0.2);

#ifndef NDEBUG
    assert(physics.shape not_eq nullptr);
    assert(physics.rigidBody not_eq nullptr);
#endif
    vehicleRayCaster = new btDefaultVehicleRaycaster(Simulation::mir.world);

    //Creates a new instance of the raycast vehicle
    btVehicle = new btRaycastVehicle(tuning, physics.rigidBody, vehicleRayCaster);

#ifndef NDEBUG
    assert(btVehicle not_eq nullptr);
    assert(Simulation::mir.world not_eq nullptr);
#endif

    //Adds the vehicle to the world
    Simulation::mir.world->addVehicle(btVehicle);

    //Adds the wheels to the vehicle
    addWheels(btVehicle, tuning);
}

void Vehicle::setEngineForce(const btScalar &f) {

    if(f not_eq 0) {
        if(steer < btScalar(0.005f) and steer > btScalar(-0.005f)) {
            steer = btScalar(0);
        } else if(steer > 0) {
            steer -= btScalar(0.005f);
        } else {
            steer += btScalar(0.005f);
        }
    }
    engineForce = f;

    btVehicle->setSteeringValue(btScalar(steer), 0);
    btVehicle->setSteeringValue(btScalar(steer), 1);

    btVehicle->applyEngineForce(engineForce, 2);
    btVehicle->applyEngineForce(engineForce, 3);
    return;
}

/*this is a really bad name and should be changed*/
void Vehicle::updateTires(const btScalar s) {

    steer += s;
    btScalar range = btScalar(.15);
    if(steer not_eq 0) {
        if(steer > range) {
            steer = range;
        } else if(steer < -range) {
            steer = -range;
        }
    }
    btVehicle->setSteeringValue(btScalar(steer), 0);
    btVehicle->setSteeringValue(btScalar(steer), 1);
}

void Vehicle::notify() {
    btScalar rate = btScalar(1000.0f);
    rearRightWheel->setRotation(irr::core::vector3df(btVehicle->getWheelInfo(
                                    0).m_rotation * rate, irr::f32(180), irr::f32(0)));
    frontLeftWheel->setRotation(irr::core::vector3df(-btVehicle->getWheelInfo(
                                    2).m_rotation * rate, irr::f32(steer * 180), irr::f32(0)));
    frontRightWheel->setRotation(irr::core::vector3df(btVehicle->getWheelInfo(
                                     3).m_rotation * rate, irr::f32(180 + (steer * 180)), irr::f32(0)));
    rearLeftWheel->setRotation(irr::core::vector3df(-btVehicle->getWheelInfo(
                                   1).m_rotation * rate, irr::f32(0), irr::f32(0)));
}

void Vehicle::addWheels(
    btRaycastVehicle *vehicle,
    btRaycastVehicle::btVehicleTuning tuning) {
    //The direction of the raycast, the btRaycastVehicle uses raycasts instead of simiulating the wheels with rigid bodies
    btVector3 wheelDirectionCS0(0, -1, 0);

    //The axis which the wheel rotates arround
    btVector3 wheelAxleCS(-1, 0, 0);

    btScalar suspensionRestLength(0.8);

    //Adds the front wheels
    vehicle->addWheel(irrToBtVector(frontRightWheelConnection), wheelDirectionCS0, wheelAxleCS, suspensionRestLength, wheelRadius, tuning, true);
    vehicle->addWheel(irrToBtVector(frontLeftWheelConnection), wheelDirectionCS0, wheelAxleCS, suspensionRestLength, wheelRadius, tuning, true);

    //Adds the rear wheels
    vehicle->addWheel(irrToBtVector(rearRightWheelConnection), wheelDirectionCS0, wheelAxleCS, suspensionRestLength, wheelRadius, tuning, false);
    vehicle->addWheel(irrToBtVector(rearLeftWheelConnection), wheelDirectionCS0, wheelAxleCS, suspensionRestLength, wheelRadius, tuning, false);

    //Configures each wheel of our vehicle, setting its friction, damping compression, etc.
    //For more details on what each parameter does, refer to the docs
    for(int i = 0; i < vehicle->getNumWheels(); i++) {
        btWheelInfo &wheel = vehicle->getWheelInfo(i);
        wheel.m_suspensionStiffness = 50;
        wheel.m_wheelsDampingCompression = btScalar(0.3) * 2 * btSqrt(wheel.m_suspensionStiffness);//btScalar(0.8);
        wheel.m_wheelsDampingCompression = btScalar(2.0f);
        wheel.m_wheelsDampingRelaxation = btScalar(3.0f);
        wheel.m_wheelsDampingRelaxation = btScalar(0.5) * 2 * btSqrt(wheel.m_suspensionStiffness);//1;
        //Larger friction slips will result in better handling
        wheel.m_frictionSlip = btScalar(120.0f);
        wheel.m_rollInfluence = 1;
    }
}
