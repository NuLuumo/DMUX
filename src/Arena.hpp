#pragma once

/**
 * @file   Arena.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @author Nicolas Ortega (nicolas.ortega.froysa@gmail.com)
 * @brief  Poorly written class that loads the map for the players
 *
 * Copyright (c) 2016 Collective Tyranny
 */

#include "comp/Graphics.hpp"
#include "comp/Physics.hpp"

/**
 * \brief  Poorly written class that loads the map for the players
 */
class Arena final {
public:

    /**
     * \brief Constructor for Arena class
     * \details initializes the arena according to the data
     * specific about the world, right now it just loads a
     * map file
     * \param wData The world data the arena is being placed in
     */
    Arena();
private:
    gal::comp::Graphics graphics;
    gal::comp::Physics physics;
};
