#pragma once
#include <cstddef>
#include <string>
#include "Logger.hpp"

namespace gal {
namespace comp {
class Subject;
} // comp
} // gal
namespace gal {

class Observer {
public:
    Observer();
    /**
     * \brief updates the Observer
     * \details Function must be overridden, this function is called once per
     * frame once the init function has been called in the derived class
     */
    virtual void notify() = 0;
    void discover(gal::comp::Subject &subject); //!< Function used to add to WorldData keyring, invokes update function each frame
    ~Observer();

protected:

    const std::string ID; //!< The UUID of the Entity

private:
    void *operator new(std::size_t);    //!< Preventing heap allocation
    void *operator new[](std::size_t);  //!< Preventing heap allocation
    void  operator delete[](void *) {} //!< Preventing heap allocation
};

} // gal
