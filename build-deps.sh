#!/usr/bin/env bash

# Build cAudio:
git submodule update --init lib/cAudio/
mkdir -p lib/cAudio/build
cd lib/cAudio/build
cmake -DCAUDIO_STATIC=TRUE -DCAUDIO_BUILD_SAMPLES=FALSE $@ ..
make
cd -

# Build Pluto:
git submodule update --init lib/pluto/
mkdir -p lib/pluto/build
cd lib/pluto/build
cmake $@ ..
make
cd -

# Build CPPLocate:
git submodule update --init lib/cpplocate/
mkdir -p lib/cpplocate/build
cd lib/cpplocate/build
cmake $@ ..
make
cd -

# Build RakNet:
git submodule update --init lib/RakNet/
mkdir -p lib/RakNet/build
cd lib/RakNet/build
cmake $@ ..
make
cd -

# Build IrrIMGUI:
git submodule update --init lib/IrrIMGUI
rm lib/IrrIMGUI/dependency/IMGUI/install_IMGUI_here.txt
git clone https://github.com/ocornut/imgui.git lib/IrrIMGUI/dependency/IMGUI
mkdir -p lib/IrrIMGUI/build
cd lib/IrrIMGUI/build
cmake  -DIRRIMGUI_STATIC_LIBRARY=ON -DIRRIMGUI_BUILD_EXAMPLES=OFF $@ ..
make
cd -
