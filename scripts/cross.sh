#!/usr/bin/env bash
current=$PWD

# How many jobs are we issuing to make
compile_jobs=4
toolchain_file=$current/scripts/windows-32-toolchain.cmake 
install_dir=$current/cross-win32
compile_dir=$install_dir/compile
include_dir=$install_dir/include

# These flags are applied to all libs, some won't be used
cmake_flags="-DIRRLICHT_LIBRARY='lib/irrlicht'
-DIRRLICHT_INCLUDE_DIR='$include_dir' 
-DENet_LIBRARY='lib/enet' 
-DENet_INCLUDE_DIRS='$include_dir' 
-DTINYXML2_LIBRARY='lib/tinyxml2' 
-DBULLET_DYNAMICS_LIBRARY='' 
-DBULLET_COLLISION_LIBRARY='' 
-DBULLET_MATH_LIBRARY='' 
-DBULLET_SOFTBODY_LIBRARY='' 
-DBULLET_INCLUDE_DIR='$include_dir' 
-DOPENAL_LIBRARY='$compile_dir/cAudio/Dependencies64' 
-DOPENAL_INCLUDE_DIR='$compile_dir/cAudio/Dependencies64/include'"

mkdir $install_dir $compile_dir $include_dir -p
mkdir $install_dir/bin -p
mkdir $current/lib -p
mkdir $include_dir -p
mkdir $current/build -p

source scripts/cross-functions.sh


cd $compile_dir
compile_tinyxml2
#compile_openal
compile_caudio
exit
compile_bullet3
compile_irrlicht
compile_irrlicht_bullet
compile_enet

cd $current
# Wrap around the build-deps script to build other libs
./build-deps.sh -DCMAKE_TOOLCHAIN_FILE=$toolchain_file -DCMAKE_INSTALL_PREFIX=$install_dir $cmake_flags

# Install all libs to the cross-lib dir
cd $compile_dir
old_dir=$PWD
for i in *; do 
  cd $i
  echo Installing $i ...
  make install
  cd $old_dir 
done;

# Hack to gather all includes - might need to be tweaked
echo "Copying headers"

# Piping to shell no-op silences any error
find $current -name "*.h" -exec cp {} $include_dir | : \;

# Compile DMUX
cd $current/build
cmake -DCMAKE_TOOLCHAIN_FILE=$toolchain_file -DCMAKE_INSTALL_PREFIX=$install_dir -DADDITIONAL_INCLUDES=$include_dir $cmake_flags ..
make -j$compile_jobs
