#!/usr/bin/dont-run

base_url="http://github.com"

clone_and_go(){
  old_dir=$PWD
  name=$(basename $1)
  echo "Cloning $name"
  if [ ! -d "$base_url/$1" ]; then
    git clone $base_url/$1 -q
  else
    cd $1
    git pull
  fi;
  mkdir $name/build
  cd $name/build
}

compile_caudio(){
  clone_and_go R4stl1n/cAudio
  cmake -DCMAKE_TOOLCHAIN_FILE=$toolchain_file \
    -DCMAKE_INSTALL_PREFIX=$install_dir \
    -DCAUDIO_DEPENDENCIES=../Dependencies64/ \
    -DCAUDIO_ENABLE_OGG=0 \
    -DCAUDIO_STATIC=1 \
    -DCAUDIO_BUILD_SAMPLES=0 $cmake_flags ..

  make install
  cd $old_dir
}

compile_enet(){
  clone_and_go lsalzman/enet
  cmake -DCMAKE_TOOLCHAIN_FILE=$toolchain_file -DCMAKE_INSTALL_PREFIX=$install_dir $cmake_flags ..
  make -j$compile_jobs install
  cd $old_dir
}

compile_bullet3(){
  clone_and_go bulletphysics/bullet3
  cmake -DCMAKE_TOOLCHAIN_FILE=$toolchain_file -DCMAKE_INSTALL_PREFIX=$install_dir $cmake_flags ..
  make -j$compile_jobs install
  cd $old_dir
}

compile_tinyxml2(){
  clone_and_go leethomason/tinyxml2
  cmake -DCMAKE_TOOLCHAIN_FILE=$toolchain_file -DCMAKE_INSTALL_PREFIX=$install_dir $cmake_flags ..
  make -j$compile_jobs install
  cd $old_dir
}

compile_openal(){
  clone_and_go kcat/openal-soft
  cmake -DCMAKE_TOOLCHAIN_FILE=$toolchain_file -DCMAKE_INSTALL_PREFIX=$install_dir $cmake_flags ..
  make -j$compile_jobs install
  cd $old_dir
}

compile_irrlicht(){
  clone_and_go Perlmint/irrlicht-cmake
  cmake -DCMAKE_TOOLCHAIN_FILE=$toolchain_file -DCMAKE_INSTALL_PREFIX=$install_dir $cmake_flags ..
  make -j$compile_jobs install
  cd $old_dir
}

compile_irrlicht_bullet(){
  clone_and_go danyalzia/irrBullet
  make -j$compile_jobs install
  cd $old_dir
}
