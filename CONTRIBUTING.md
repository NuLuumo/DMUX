Contributing
============
If you wish to contribute to DMUX please use the LLVM coding style or run the `vnao.sh` script before commiting. We usually don't care too much about style, the only two aspects of coding style that we care about the most are 4 space indentation (no tab characters) and curly brackets are attached.

After having contributed feel free to add yourself to the [CONTRIBUTORS file](/CONTRIBUTORS.md).

How to License Your Contributions
---------------------------------
Please take into account that when contributing to DMUX all your code will be licensed under the respective license (if you contribute to the client then it will be the [GNU GPLv3](/src/client/LICENSE) and if you contribute to the server it will be the [GNU Affero GPLv3](/src/client/LICENSE)). All asset contributions please add them to the [DMUX-Assets repo](https://gitlab.com/CollectiveTyranny/DMUX-Assets), in which case your assets **must use a free culture license** that allows redistribution and commercial use (for example, both CC-BY and CC-BY-SA are acceptable licenses for assets, but CC-BY-ND or CC-BY-NC are not).

Please remember that DMUX follows the principles of [free software](https://www.gnu.org/philosophy/free-sw.en.html).

Our To-Do List
--------------
As you may notice, we don't have a To-Do list, instead we have the [Wishlist](WISHLIST.txt). This is because we tried doing a To-Do list but we found lack of motivation to maintain it. This wishlist is simply a list of things that the developers want done and anyone can complete these wishes. So use that file as a To-Do list.

Joining the DMUX Team
---------------------
The way we work is rather informal, but basically, if we see someone that is contributing a lot to DMUX we will most likely end up asking them if they want to join the team. So if you want to join the DMUX team just contribute a lot and hope Keys-senpai notices you.
