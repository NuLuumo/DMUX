![DMUX](assets/DMUX.png "DMUX")
====
DMUX is an online multiplayer derby style combat game where teams face off in slightly customized vehicles playing various game modes.

A 3rd part of Zombie AI that are hostile to both teams is also present to balance gameplay. The Zombies have hostility that varies between the two teams, the losing players are less likely to be attacked by the zombies while the winning players are targetted more often. The objective of this is to ensure that the game is challenging and engaging for experienced and non-experienced players alike.

DMUX is unique because it will contain a game mode called DMUX Universe, an infinitely spanning universe of servers. This mode does not have objectives, you simply connect to one of the DMUX Universe servers and from there you can enter portals to others.

DMUX also features basic customization, the user selects a weapon, a chassis, and a tire set. The focus is not on hyper-customization of a vehicle but rather on the gameplay itself. There is no layout that is better than another, it's simply what the player feels more comfortable using.

The most impressive feature of DMUX is that it is 100% free/libre software! All the code to DMUX and the libraries used to create it are licensed with free software licenses such as the ZLib license, while DMUX itself is licensed with the [GNU AGPLv3](/LICENSE).

Building
========
This project is built with [CMake](https://cmake.org/) in order to help with the cross-compatibility of the build. We have the following libraries as dependencies so please make sure you have the development packages installed for them:
 - OpenAL
 - OpenGL
 - Irrlicht
 - Bullet
 - TinyXML2
 - ZLIB

GNU/Linux
---------
We currently only support building on Debian, Arch and Fedora based GNU/Linux systems. If you would like to add support for compiling on other distros we welcome your contribution.

To install the dependencies on a dpkg based system run the following command (if you're using Ubuntu add `sudo` at the beginning of the command):
```bash
$ apt-get install build-essential cmake libopenal-dev libirrlicht-dev libbullet-dev libenet-dev libtinyxml2-dev libgl1-mesa-dev
```

To install the dependencies on an RPM based system run the following command:
```bash
$ dnf install gcc-c++ cmake openal-soft-devel mesa-libGL\*-devel irrlicht-devel bullet-devel tinyxml2-devel enet-devel
```

To install the dependencies on a pacman based system run the following command:
```bash
$ pacman -S make cmake gcc bullet irrlicht tinyxml2 enet
```

To compile the game run the following commands:
```bash
$ ./build-deps.sh
$ cd build/
$ cmake ..
$ make
```

Mac OSX
-------
_Coming soon._

Windows
-------
_Coming soon._

Contributing
============
If you wish to contribute to the project look at our [CONTRIBUTING file](/CONTRIBUTING.md). Take into account we are willing to accept just about any kind of contribution: 3D models, code, sound effects, music, even helping to build support for other platforms we don't have specified here (such as any of the BSD distros, or Gentoo, or any other platform you want DMUX to run on). If you want to add support for another platform that has different build instructions than those specified above, simply add it to the above list so others can compile the project as well.

Making new vehicles
===================
Want to make a new vehicle for DMUX? Great! We use the Super Tux Kart exporter to generate the needed XML to apply the attributes of our vehicle. So please [use this guide](http://supertuxkart.sourceforge.net/Kart_Creation_Tutorial) to create a vehicle for DMUX. We will have to make a modified guide for this later.

Licensing
=========
DMUX is licensed as free (libre) software. All code in this project is licensed with the [GNU Affero GPLv3](/LICENSE). We chose the Affero GPL because it counts network usage as distribution, meaning that if you make a derivative of our server and you wish to have it publicly available (connecting to the master server) you must share your modifications with the community.

All artwork made by CollectiveTyranny are licensed with CC-BY-SA 4.0 International.
